import socketIO from "socket.io";
import { knex } from "../util/db";

export let io: socketIO.Server;

export function setSocketIO(value: socketIO.Server) {
	io = value;
	// io.on The first handler to run when the socket is first connected to the server
	io.on("connection", function (socket) {
		console.log(socket.id,'is connected')
		// The handler to listen for the event_key and handle the incoming data with the handler function
		socket.on("join-room", (socketCardId) => {
			socket.join(socketCardId.toString());
		});

		socket.on('part_A',async (formObject)=>{
			await knex('comments').insert([{user_id:'1',card_id:formObject.socketCardId,comment:formObject.comment}])
			socket.to(formObject.socketCardId.toString()).emit('part_B',formObject)
		})

	});
}

// emit = send
// on = receive something

// 1. card-details = emit(comment,roomID) to server side 
// 2. server side = socket.on receive(comment,roomID)
// 3. server side = socket.to(roomID) emit(comment,roomID) to card-details
// 4. front-end = socket.on receive => html ++