export class HttpError extends Error {
	constructor(public status: number, public message: string) {
		super(message);
	}
}

export type User = {
	id: number;
	username: string;
	email?: string;
	password?: string;
	avatar: string;
	role: string;
};

export type card = {
	id: number;
	api_id: number;
	name: string;
	type: string;
	desc?: string;
	atk?: number;
	def?: number;
	banlist: string;
	level: number;
	race: string;
	attribute: string;
	linkval: number;
	linkmarkers: string;
	archetype: string;
	scale: number;
};

export type GoogleInfo = {
	id: string;
	email: string;
	verified_email: boolean;
	name: string;
	given_name: string;
	family_name: string;
	picture: string;
	locale: string;
};

export type UserField = {
	username?: string;
	password: string;
	confirmPassword: string;
};

// export class UserUpdate extends UserField {

// }
export interface Cards {
	id: Number;
	name: String;
	type: String;
	desc: String;
	race: String;
	archetype?: String;
	card_sets?: String;
	card_images: Array<object>;
	card_prices: Array<object>;
	atk?: Number;
	def?: Number;
	level?: Number;
	attribute?: String;
	banlist_info?: Object;
	scale?: Number;
	linkval?: Number;
	linkmarkers?: Array<String>;
}

export interface Users {
	username: String;
	password: String;
	email: String;
	role: String;
	avatar?: String;
}

export interface User_history {
	user: Number;
	card: Number;
	price: Number;
	buyer_id: Number;
	seller_id: Number;
}

export interface Comment {
	user: Number;
	card: Number;
	comment: String;
}
