import express from "express";
import http from "http";
import { attachApi } from "./api";
import { oAuthMiddleWare, sessionMiddleWare } from "./middleware/middleware";
import env from "./util/env";
import { logger } from "./util/logger";
import { Server as SocketIO } from "socket.io";
import { setSocketIO } from "./util/socketio";

//express server
let app = express();

//socketIO
const server = new http.Server(app);
const io = new SocketIO(server);
setSocketIO(io);

// app.use((req, res, next) => {
// 	console.log(`${req.method} /${req.path}`)
// 	next()
// })

app.use(express.json({
	limit:"50mb"
}));
app.use(sessionMiddleWare);
app.use(oAuthMiddleWare);

//use formidable

app.use(express.static("public"));
// app.use(express.static("assets/uploads"))
app.use(express.static("assets/images"));
app.use(express.static("assets/uploads"));
attachApi(app, io);

// app.use(isLoggedIn, express.static("protected"));

server.listen(env.PORT, () => {
	logger.info(`Listening at port http://localhost:${env.PORT}`);
});
