// window.onload = initLoginPage
registerFormElem = document.querySelector("#register-form")
loginFormElem = document.querySelector("#sign-in-form")

loginFormElem.addEventListener('submit', async(e) => {
    e.preventDefault()
    let loginFormObj = {
        username: loginFormElem.loginEmail.value,
        password: loginFormElem.loginPassword.value
    }

    let res = await fetch('/user/login', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(loginFormObj)
    })

    let result = await res.json()
    console.log('result= ', result)
    loginResultElem.innerText = result.message

    if (res.ok) {
        loginResultElem.classList.add('Success')
        setTimeout(() => {
            window.location = '/index.html'
        }, 1500)
    } else {
        loginResultElem.classList.add('Error')
    }
})

function showRegister() {
    console.log('showRegister')
    registerFormElem.style.display = 'block'
    loginFormElem.style.display = 'none'
}

let registerResultElem = document.querySelector('#register-result')
let loginResultElem = document.querySelector('#login-result')

async function register() {
    let registerFormObj = {
        email: registerFormElem.registerEmail.value,
        username: registerFormElem.registerUsername.value,
        password: registerFormElem.registerPassword.value
    }
    console.log('register form is submitted')

    let res = await fetch("/user/register", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(registerFormObj)
    })

    let result = await res.json()
    registerResultElem.innerText = result.message

    if (res.ok) {
        registerResultElem.classList.add('Success')
        setTimeout(() => {
            window.location = '/index.html'
        }, 1500)
    } else {
        registerResultElem.classList.add('Error')
    }
}