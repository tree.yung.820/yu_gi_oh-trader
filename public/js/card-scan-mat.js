let cardScannerElem = document.querySelector('uk-child-width-expand@m')
cardScannerElem.innerHTML = ""

async function fetchScanCard() {
    let scan_card_details = []
    
    cardScannerElem.innerHTML =
  /*html*/`
    <div class="lock">
    <div class="content">
        <h1>Scan Cards</h1><br>
        <div class="videos">
            <video id="videoInput" width="320" height="240" playsinline autoplay></video>
           <!--<canvas id="canvasOutput" width="320" height="240"></canvas>-->
        </div>

        <form id="uploadForm">
            <input type="file" name="image" id="image">
            <input type="submit" value="update" id="uploadButton">
        </form>
        <button class="take-picture" type="submit" onclick="cameraBtn()" id="uploadButton">take picture</button>
    </div>
</div>

<div class="card-detail-padding">
    <img src="${scan_card_details.image_url}"
        class="card-detail-image-size" alt="">
</div>
<div id="card-detail-data">
<div>Name: ${scan_card_details.name}</div>
<div>Level : ${scan_card_details.level}</div>
<div>Number: ${scan_card_details.api_id}</div>
<div>Attribute: ${scan_card_details.attribute}</div>
<div>Card Type: ${scan_card_details.type}</div>
<div>Race : ${scan_card_details.race}</div>
<div>Archetype: ${scan_card_details.archetype}</div>
<div>ATK : ${scan_card_details.atk}</div>
<div>DEF: ${scan_card_details.def}</div>
<div>Content: ${scan_card_details.desc}
</div>
<br><a href="card-detail.html?cardId=${scan_card_details.id}">more details....</a>
    `

}