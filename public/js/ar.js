class MarkerEffectDisplayer {
    constructor(cv) {
        this.cv = cv
    }

    cannyEdge(src, dst) {
        cv.cvtColor(src, src, cv.COLOR_RGB2GRAY, 0);
        cv.Canny(src, dst, 50, 100, 3, false);
    }
}
class DisplayData {
    constructor(video, effect) {
        let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);
        let dst = new cv.Mat(video.height, video.width, cv.CV_8UC1);
        let eft = new cv.Mat(effect.height, effect.width, cv.CV_8UC4);
        this.src = src
        this.dst = dst
        this.eft = eft
    }
    delete() {
        this.src.delete();
        this.dst.delete();
        this.eft.delete();
    }
}
class WarpPerspectiveMatrixs {
    original;
    prespertive;
    get(prespertive, width, height) {
        let o = cv.matFromArray(4, 1, cv.CV_32FC2, [
            prespertive[0][0],
            prespertive[0][1],
            prespertive[1][0],
            prespertive[1][1],
            prespertive[2][0],
            prespertive[2][1],
            prespertive[3][0],
            prespertive[3][1],
        ]);

        let p = cv.matFromArray(4, 1, cv.CV_32FC2, [
            0,
            0,
            0,
            height,
            width,
            0,
            width,
            height,
        ]);
        this.original = o;
        this.prespertive = p;
    }

    delete() {
        this.original.delete();
        this.prespertive.delete();
    }
}