let video, effect1
let printedImage = false
let haveCard = false
window.onload = () => {
    // Load opencv web assembly
    cv['onRuntimeInitialized'] = () => {
        video = document.getElementById('videoInput');
        effect1 = document.getElementById('marker1');
        effect2 = document.getElementById('marker2');
        // console.log(video.height, video.width)
        navigator.mediaDevices.getUserMedia({ video: true, audio: false })
            .then(function(stream) {
                video.srcObject = stream;
                video.play();
            })
            .catch(function(err) {
                console.log("An error occurred! " + err);
            });
        let cap = new cv.VideoCapture(video);
        let data = new DisplayData(video, effect1)
        const FPS = 30;

        function processVideo() {
            try {
                haveCard = false
                let begin = Date.now();
                // start processing.
                data.delete();
                data = new DisplayData(video, effect1)
                cap.read(data.src);
                cv.imshow('uploadCanvas', data.src.clone());
                data.dst = data.src.clone();
                try {
                    data.eft = cv.imread('effect1');
                    frame(data);
                    cv.imshow('canvasOutput', data.dst);
                    serverCardRecognition()
                } catch (e) {}
                let delay = 1000 / FPS - (Date.now() - begin);
                setTimeout(processVideo, delay);
            } catch (err) {
                console.log(err)
            }
        };

        // schedule the first one.
        setTimeout(processVideo, 0);
    }

}
let ai_working = false
async function serverCardRecognition() {
    if (printedImage === false && haveCard && !ai_working) {
        ai_working = true
            // printedImage = true
        let canvasOutput = document.getElementById("uploadCanvas")
        let image_base64 = canvasOutput.toDataURL();
        image_base64 = image_base64.replace(/^data:image\/png;base64,/, "");
        // console.log(image_base64)
        let res = await fetch('/card/scanner/canvas', {
            method: 'POST',
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({ "image_base64": image_base64 })
        })
        let ai_result = await res.json()
        if (ai_result.classname) {
            console.log(ai_result)
            let effect1 = document.getElementById("effect1")
            while (ai_result.classname[0] === "0") ai_result.classname = ai_result.classname.substring(1)
            effect1.src = `small_pic/${ai_result.classname}.jpg`
                // effect2.src = `small_pic/${ai_result.classname}.jpg`
        }
        ai_working = false
    }

}
let color = null;
let width = 480,
    height = width

function frame(data) {
    let src = data.src;
    let dst = data.dst;
    let eft = data.eft
    let edited = src.clone();
    filter(edited);
    //Demo
    // let edited = new cv.Mat(video.height, video.width, cv.CV_8UC4);
    //Demo
    // edited.copyTo(dst);
    // return;
    //Demo
    // cv.GaussianBlur(src, dst, new cv.Size(7, 7), 3);
    // return;
    //Demo
    // cv.cvtColor(edited, dst, cv.COLOR_RGBA2GRAY);
    // return;
    //Demo
    // cv.adaptiveThreshold(edited, dst, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2);
    // return;
    //Demo
    // cv.bitwise_not(edited, dst);
    // return;
    let contours = new cv.MatVector();
    let hierarchy = new cv.Mat();
    cv.findContours(edited, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
    //Demo
    // let color = new cv.Scalar(255, 255, 255, 255);
    // for (let i = 0; i < contours.size(); i++)
    //     cv.drawContours(dst, contours, i, color, 1, cv.LINE_8, hierarchy, 100);
    // return;
    contours = grab_contours(contours);
    let cas = [];
    for (let i = 0; i < contours.size(); i++) {
        let ca = {};
        ca.area = cv.contourArea(contours.get(i));
        ca.contour = contours.get(i);
        if (ca.area > 5000) {
            cas.push(ca);
            //Demo
            // let color = new cv.Scalar(255, 255, 255, 255);
            // cv.drawContours(dst, contours, i, color, 1, cv.LINE_8, hierarchy, 100);
        }
    }
    // return
    cas.sort(areaCompare)
    let rects = []
    let cav = new cv.MatVector();
    let scas = []
    for (let ca of cas) {
        let arcl = cv.arcLength(ca.contour, true);
        let m = new cv.Mat()
        cv.approxPolyDP(ca.contour, m, 0.02 * arcl, true);
        if (m.size().height == 4) {
            scas.push(ca)
            let rect = []
            cav.push_back(m)
            for (let i = 0; i < m.size().height; i++) {
                rect.push(m.intPtr(0, i));
            }
            rects.push(rect)
                // break
        }
        m.delete();
    }
    if (rects.length == 0) {
        cav.delete();
        contours.delete();
        hierarchy.delete();
        edited.delete();
        deleteAll(scas);
        return;
    }
    if (color == null) color = new cv.Scalar(0, 255, 0, 255);
    // cv.drawContours(dst, cav, 0, color, 2);
    let ordered = reorderRectCorners(rects[0])
    let pair = getWarpPerspectiveMatrixs(ordered, effect1.width, effect1.height);
    // let M = cv.getPerspectiveTransform(pair.original, pair.prespertive);
    let M1 = cv.getPerspectiveTransform(pair.prespertive, pair.original);
    let M2 = cv.getPerspectiveTransform(pair.original, pair.prespertive);
    let dsize = new cv.Size(video.width, video.height);
    let wraped = new cv.Mat();
    // let i = imageRecognition(src, dst, eft, M2);
    let i = 2
    let bs = 21,
        C = 2;
    filter(dst, bs, C);
    cv.imread('effect1').copyTo(eft);
    haveCard = true
    cv.warpPerspective(
        eft,
        wraped,
        M1,
        dsize,
        cv.INTER_LINEAR,
        cv.BORDER_CONSTANT,
        new cv.Scalar()
    );
    //Demo
    // cv.warpPerspective(
    //     eft,
    //     dst,
    //     M1,
    //     dsize,
    //     cv.INTER_LINEAR,
    //     cv.BORDER_CONSTANT,
    //     new cv.Scalar()
    // );
    // return;
    let aug = new cv.Mat();
    let mask = cv.Mat.zeros(effect1.height, effect1.width, cv.CV_8UC1);
    cv.bitwise_not(mask, mask)
    cv.warpPerspective(mask, mask, M1, dsize, cv.INTER_LINEAR, cv.BORDER_CONSTANT, new cv.Scalar());
    if (i == 1) {
        cv.bitwise_or(wraped, src, dst)
    }
    if (i == 2) {
        let maskV = cv.Mat.zeros(video.height, video.width, cv.CV_8UC1);
        cv.bitwise_not(maskV, maskV)
        cv.bitwise_not(mask, mask)
        cv.bitwise_or(wraped, src, dst, mask)
            // cv.imshow('canvasOutput', maskV);
        cv.add(wraped, dst, dst, maskV, cv.CV_8UC4)
        maskV.delete();
    }
    mask.delete();
    aug.delete();
    wraped.delete();
    M1.delete();
    M2.delete();
    pair.delete();
    cav.delete();
    contours.delete();
    hierarchy.delete();
    edited.delete();
    deleteAll(scas)
}

function areaCompare(a, b) {
    return b.area - a.area
}

function grab_contours(cnts) {
    if (cnts.size() == 2) {
        cnts = cnts.get(0);
    } else if (cnts.size() == 3) {
        cnts = cnts.get(1);
    }
    return cnts;
}

function reorderRectCorners(points) {
    let ordered = [...points];
    for (let i = 1; i < ordered.length; i++) {
        for (let j = 0; j < i; j++) {
            if (ordered[i][0] < ordered[j][0]) {
                let x = ordered[i];
                ordered[i] = ordered[j];
                ordered[j] = x;
            }
        }
    }
    if (ordered[1][1] < ordered[0][1]) {
        let k = ordered[1];
        ordered[1] = ordered[0];
        ordered[0] = k;
    }
    if (ordered[3][1] < ordered[2][1]) {
        let t = ordered[3];
        ordered[3] = ordered[2];
        ordered[2] = t;
    }
    return ordered;
}

function getWarpPerspectiveMatrixs(prespertive, width, height) {
    let pair = new WarpPerspectiveMatrixs();
    pair.get(prespertive, width, height);
    return pair;
}

function filter(edited, blockSize = 11, C = 2) {
    cv.GaussianBlur(edited, edited, new cv.Size(7, 7), 3);
    cv.cvtColor(edited, edited, cv.COLOR_RGBA2GRAY);
    cv.adaptiveThreshold(
        edited,
        edited,
        255,
        cv.ADAPTIVE_THRESH_GAUSSIAN_C,
        cv.THRESH_BINARY,
        blockSize,
        C
    );
    cv.bitwise_not(edited, edited);
    return edited;
}

function imageRecognition(src, dst, eft, M) {
    let width = effect1.width;
    let height = effect1.height;
    let dsize = new cv.Size(width, height);
    // let eft1 = cv.imread('marker1');
    // let eft2 = cv.imread('marker2');
    cv.warpPerspective(
        src,
        dst,
        M,
        dsize,
        cv.INTER_LINEAR,
        cv.BORDER_CONSTANT,
        new cv.Scalar()
    );
    // return;
    let bs = 21,
        C = 2;
    filter(dst, bs, C);
    // filter(eft1, bs, C);
    // filter(eft2, bs, C);
    // let mask = cv.Mat.zeros(height, width, cv.CV_8UC1);
    // let cmp = new cv.Mat();
    // cv.bitwise_xor(dst, eft1, cmp);
    // // let a = cv.countNonZero(cmp);
    // cmp.copyTo(dst)
    //     // return;
    // cv.bitwise_xor(dst, eft2, cmp);
    // // let b = cv.countNonZero(cmp);
    // cmp.copyTo(dst)
    // return;
    // cmp.delete();
    // mask.delete();
    // eft1.delete();
    // eft2.delete();
    cv.imread('effect1').copyTo(eft);
    haveCard = true
    return 2
        // if (a < b) {
        //     cv.imread('effect1').copyTo(eft);
        //     return 1;
        // } else {
        //     cv.imread('effect2').copyTo(eft);
        //     return 2;
        // }
}

function analysis(src) {
    return cv.countNonZero(src);
}

function deleteAll(arr) {
    for (a of arr) {
        a.contour.delete()
    }
}