// fetch
async function fetchUser() {
  let res = await fetch("/user");
  if (res.ok) {
    const result = await res.json();
    return result;
  } else {
    return 'fetch user is down';
  }
}

async function login() {
  let user = await fetchUser()
  console.log('user from index', user );
  if (user.info != null) {
    const { id, username, avatar } = user
    console.log(id, username, avatar)
    document.querySelector(".sign-in-line-style").innerHTML = `
      <a class="uk-button uk-button-default loginColor" href="#" type="button" id="dropdownMenuLink">
                  <img src="/test-avatar.jpeg" class="avatar" alt="Avatar">
                  <span>${user.info.username}</span>
      </a>
      
      <div uk-dropdown="pos: bottom-justify">
                  <ul class="uk-nav uk-dropdown-nav">
                    <li><a class="uk-active" href="#">Profile</a></li>
                    <li><a class="uk-active" href="#">Cart</a></li>
                    <li><a class="uk-active" onclick="logout()" href="index.html">Log Out</a></li>
                  </ul>
      </div>
    `
  } else {
    document.querySelector('.sign-in-line-style').innerHTML = `
    <a class="sign-in-line-style non-logIn-style log-in-decoration" href="/sign-in.html">
                    Sign In
                  </a>
    `
  }
}

async function logout(){

  await fetch('/user/logout', { method: 'get' })  

}

//slider function

let output = document.querySelector('#output-value')
let slider = document.querySelector('.uk-range')
output.innerHTML = slider.value;

slider.oninput = () => {
  output.innerHTML = slider.value;
  output.style.width = slider.value;
}
// -----fetch Cards-----
let Index_NLC_ContainerElem = document.querySelector(`#NLC`)
let Index_TS_ContainerElem = document.querySelector(`#TS`)
let Index_RS_ContainerElem = document.querySelector(`#RS`)

Index_NLC_ContainerElem.innerHTML = ""
Index_TS_ContainerElem.innerHTML = ""
Index_RS_ContainerElem.innerHTML = ""

async function fetchIndexCards() {
  let result = await fetch('/indexPage')
  let result_JSON = await result.json()
  let cards_NLC = result_JSON.data_NLC
  let cards_TS = result_JSON.data_TS
  let cards_RS = result_JSON.data_RS

  // ------AREA ONE------
  let cards_NLC_HTML = /*html*/`
<div class="uk-flex uk-flex-center">
<a href="card-detail.html?cardId=${cards_NLC[0].card_id}">
  <img src="${cards_NLC[0].image_url}" class="cards-size" alt="">
</a>
<p>
  ${cards_NLC[0].name}
</p>
<div>USD $ ${cards_NLC[0].price}</div>
<span class="cart-btn" uk-icon="cart">Add to Cart</span>
</div>
<div>
<a href="card-detail.html?cardId=${cards_NLC[1].card_id}">
  <img src="${cards_NLC[1].image_url}" class="cards-size" alt="">
</a>
<p>
  ${cards_NLC[1].name}
</p>
<div>USD $ ${cards_NLC[1].price}</div>
<span class="cart-btn" uk-icon="cart">Add to Cart</span>
</div>
<div>
<a href="card-detail.html?cardId=${cards_NLC[2].card_id}">
  <img src="${cards_NLC[2].image_url}" class="cards-size" alt="">
</a>
<p>
  ${cards_NLC[2].name}
</p>
<div>USD $ ${cards_NLC[2].price}</div>
<span class="cart-btn" uk-icon="cart">Add to Cart</span>
</div>
`
  Index_NLC_ContainerElem.innerHTML = cards_NLC_HTML

  // ------AREA TWO------
  let cards_TS_HTML = /*html*/`
  <div class="uk-flex uk-flex-center">
  <a href="card-detail.html?cardId=${cards_TS[0].card_id}">
    <img src="${cards_TS[0].image_url}" class="cards-size" alt="">
  </a>
  <p>
    ${cards_TS[0].name}
  </p>
  <div>USD $ ${cards_TS[0].price}</div>
  <span class="cart-btn" uk-icon="cart">Add to Cart</span>
  </div>
  <div>
  <a href="card-detail.html?cardId=${cards_TS[1].card_id}">
    <img src="${cards_TS[1].image_url}" class="cards-size" alt="">
  </a>
  <p>
    ${cards_TS[1].name}
  </p>
  <div>USD $ ${cards_TS[1].price}</div>
  <span class="cart-btn" uk-icon="cart">Add to Cart</span>
  </div>
  <div>
  <a href="card-detail.html?cardId=${cards_TS[2].card_id}"">
    <img src="${cards_TS[2].image_url}" class="cards-size" alt="">
  </a>
  <p>
    ${cards_TS[2].name}
  </p>
  <div>USD $ ${cards_TS[2].price}</div>
  <span class="cart-btn" uk-icon="cart">Add to Cart</span>
  </div>
  `
  Index_TS_ContainerElem.innerHTML = cards_TS_HTML

  // ------AREA THREE------
  let cards_RS_HTML = /*html*/`
    <div class="uk-flex uk-flex-center">
    <a href="card-detail.html?cardId=${cards_RS[0].card_id}">
      <img src="${cards_RS[0].image_url}" class="cards-size" alt="">
    </a>
    <p>
      ${cards_RS[0].name}
    </p>
    <div>USD $ ${cards_RS[0].price}</div>
    <span class="cart-btn" uk-icon="cart">Add to Cart</span>
    </div>
    <div>
    <a href="card-detail.html?cardId=${cards_RS[1].card_id}">
      <img src="${cards_RS[1].image_url}" class="cards-size" alt="">
    </a>
    <p>
      ${cards_RS[1].name}
    </p>
    <div>USD $ ${cards_RS[1].price}</div>
    <span class="cart-btn" uk-icon="cart">Add to Cart</span>
    </div>
    <div>
    <a href="card-detail.html?cardId=${cards_RS[2].card_id}">
      <img src="${cards_RS[2].image_url}" class="cards-size" alt="">
    </a>
    <p>
      ${cards_RS[2].name}
    </p>
    <div>USD $ ${cards_RS[2].price}</div>
    <span class="cart-btn" uk-icon="cart">Add to Cart</span>
    </div>
    `
  Index_RS_ContainerElem.innerHTML = cards_RS_HTML
  
}

function initIndex() {
  fetchIndexCards()
  login()
}

initIndex()
