//slider function

let output = document.querySelector('#output-value')
let slider = document.querySelector('.uk-range')
output.innerHTML = slider.value;

slider.oninput = () => {
  output.innerHTML = slider.value;
  output.style.width = slider.value;
}

// fetch card-market cards
let cardMarketContainerElem = document.querySelector('.cards-market-container')
cardMarketContainerElem.innerHTML = ""

async function fetchCardMarkets() {
  let search = new URLSearchParams(window.location.search)
  let selectedPageId = search.get('page')
  console.log('selected page =', selectedPageId)
  if (selectedPageId !== null) {
    let result = await fetch(`/card-market/${selectedPageId}`)
    let result_JSON = await result.json()
    let cardMarketCards = result_JSON.data
    console.log(`card_market_cards =`, cardMarketCards)

    for (let i = 0; i < cardMarketCards.length; i++) {
      cardMarketCards_HTML =
      /*html*/`
        <div class="uk-card uk-card-default uk-card-hover uk-card-body">
          <a href="card-detail.html?cardId=${cardMarketCards[i].card_id}">
            <img src="${cardMarketCards[i].image_url}" class="cards-size" alt="">
          </a>
          <p>
            ${cardMarketCards[i].name}
          </p>
          <div>USD $ ${cardMarketCards[i].price}</div>
          <span class="cart-btn" uk-icon="cart">Add to Cart</span>
        </div>
      `
      cardMarketContainerElem.innerHTML += cardMarketCards_HTML
    }
  }

}


let cardMarketPagesElem = document.querySelector('.uk-pagination')
cardMarketPagesElem.innerHTML = ""
let cardMarketPageNumber = document.querySelector('.uk-breadcrumb li:last-child a')
cardMarketPageNumber.innerHTML = ""

async function cardMarketPages() {
  let search = new URLSearchParams(window.location.search)
  let selectedPageId = search.get('page')
  let searchName = search.get('searchName')
  if (selectedPageId !== null) {
    cardMarketPageNumber.innerHTML = `Card Market / ${selectedPageId}`
  } else {
    cardMarketPageNumber.innerHTML = `Card Market / ${searchName}`
  }

  let cardPage_HTML =
    /*html*/`
    <li><a href="card-market.html?page=${+selectedPageId - 1}"><span uk-pagination-previous></span></a></li>
    <li><a href="card-market.html?page=1">1</a></li>
    <li><a href="card-market.html?page=2">2</a></li>
    <li><a href="card-market.html?page=3">3</a></li>
    <li><a href="card-market.html?page=4">4</a></li>
    <li class="uk-disabled"><span>...</span></li>
    <li><a href="card-market.html?page=746">746</a></li>
    <li><a href="card-market.html?page=${+selectedPageId + 1}"><span uk-pagination-next></span></a></li>
    `
  cardMarketPagesElem.innerHTML += cardPage_HTML
}


async function cardSearch() {
  let search = new URLSearchParams(window.location.search)
  let searchName = search.get('searchName')
  // console.log('searchName =', searchName)
  let result = await fetch(`/card-market/search/${searchName}`)
  let result_JSON = await result.json()
  let searchedCard = result_JSON.data
  console.log(`searched card =`, searchedCard)

  if (searchedCard.length !== 0) {
    for (let i = 0; i < searchedCard.length; i++) {
      cardMarketCards_HTML =
    /*html*/`
      <div class="uk-card uk-card-default uk-card-hover uk-card-body">
        <a href="card-detail.html?cardId=${searchedCard[i].card_id}">
          <img src="${searchedCard[i].image_url}" class="cards-size" alt="">
        </a>
        <p>
          ${searchedCard[i].name}
        </p>
        <div>USD $ ${searchedCard[i].price}</div>
        <span class="cart-btn" uk-icon="cart">Add to Cart</span>
      </div>
    `
      cardMarketContainerElem.innerHTML += cardMarketCards_HTML
      cardMarketPagesElem.innerHTML = ""
    }
  } else {
    return
  }

}

async function cardFilter() {
  let filterForm = document.querySelector('#filter-form')
  filterForm.addEventListener('submit', async function (event) {
    event.preventDefault()

    const form = event.target
    const formObject = {}
    formObject['selectCardType'] = form.selectCardType.value
    formObject['selectRace'] = form.selectRace.value
    formObject['selectAttribute'] = form.selectAttribute.value
    // formObject['selectCardEffect'] = form.selectCardEffect.value
    // formObject['selectBanList'] = form.selectBanList.value
    formObject['selectCardArchetype'] = form.selectCardArchetype.value
    formObject['selectFormat'] = form.selectFormat.value
    formObject['selectStar'] = form.selectStar.value
    formObject['cardAtk'] = form.cardAtk.value
    formObject['cardDef'] = form.cardDef.value
    formObject['linkValue'] = form.linkValue.value
    formObject['scaleValue'] = form.scaleValue.value
    formObject['cardPrice'] = form.cardPrice.value
    // console.log("selected filter element: ", formObject)

    const res = await fetch('/filter', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formObject)
    })
    const result_JSON = await res.json()

    let filterCard = result_JSON.data
    console.log('filtered Card =', filterCard)
    cardMarketContainerElem.innerHTML = ""

    if (filterCard.length !== 0) {
      for (let i = 0; i < filterCard.length; i++) {
        cardMarketCards_HTML =
      /*html*/`
        <div class="uk-card uk-card-default uk-card-hover uk-card-body">
          <a href="card-detail.html?cardId=${filterCard[i].card_id}">
            <img src="${filterCard[i].image_url}" class="cards-size" alt="">
          </a>
          <p>
            ${filterCard[i].name}
          </p>
          <div>USD $ ${filterCard[i].price}</div>
          <span class="cart-btn" uk-icon="cart">Add to Cart</span>
        </div>
      `
        cardMarketContainerElem.innerHTML += cardMarketCards_HTML
        cardMarketPagesElem.innerHTML = ""
      }
    } else {
      return
    }
    cardMarketPageNumber.innerHTML = `Card Market`
  })
}

// fetch User
async function fetchUser() {
  let res = await fetch("/user");
  if (res.ok) {
    const result = await res.json();
    return result;
  } else {
    return 'fetch user is down';
  }
}

async function login() {
  let user = await fetchUser()
  console.log('user from index', user );
  if (user.info != null) {
    const { id, username, avatar } = user
    console.log(id, username, avatar)
    document.querySelector(".sign-in-line-style").innerHTML = `
      <a class="uk-button uk-button-default loginColor" href="#" type="button" id="dropdownMenuLink">
                  <img src="/test-avatar.jpeg" class="avatar" alt="Avatar">
                  <span>${user.info.username}</span>
      </a>
      
      <div uk-dropdown="pos: bottom-justify">
                  <ul class="uk-nav uk-dropdown-nav">
                    <li><a class="uk-active" href="#">Profile</a></li>
                    <li><a class="uk-active" href="#">Cart</a></li>
                    <li><a class="uk-active" onclick="logout()" href="index.html">Log Out</a></li>
                  </ul>
      </div>
    `
  } else {
    document.querySelector('.sign-in-line-style').innerHTML = `
    <a class="sign-in-line-style non-logIn-style log-in-decoration" href="/sign-in.html">
                    Sign In
                  </a>
    `
  }
}

async function logout(){

  await fetch('/user/logout', { method: 'get' })  

}

async function initCardMarket() {
  cardMarketPages()
  fetchCardMarkets()
  cardSearch()
  cardFilter()
  login()
}

initCardMarket()