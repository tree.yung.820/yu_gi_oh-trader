const video = document.querySelector('#videoInput');
const canvas = document.querySelector('#canvasOutput');
const snap = document.querySelector('#snap');
const errorMsgElement = document.getElementById('span#ErrorMsg');

const constraints = {
  audio: false,
  video: {
    width: 320,
    height: 240
  }
};

async function init(){
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints)
    handleSuccess(stream)
  }catch (e){
    errorMsgElement.innerHTML = `navigator.getUserMedia.error: ${e.toString()}`
  }
}

function handleSuccess(stream){
  window.stream = stream;
  video.srcObject = stream;
}

init();

let context = canvas.getContext('2d');
snap.addEventListener("click", function (){
  context.drawImage(video, 0, 0, 640, 480)
})