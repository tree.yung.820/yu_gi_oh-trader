// fetch card-details 
let cardDetailsContainerElem = document.querySelector('main')
cardDetailsContainerElem.innerHTML = ""
let searchId = new URLSearchParams(window.location.search)
let selectedSocketCardId = searchId.get('cardId')

async function fetchCardDetails() {
    let search = new URLSearchParams(window.location.search)
    let selectedCardId = search.get('cardId')
    console.log('selected card ID =', selectedCardId)

    let result = await fetch(`/card-detail/${selectedCardId}`)
    let result_JSON = await result.json()
    let card_details = result_JSON.result_card_details[0]
    let card_prices = result_JSON.result_card_prices
    let card_price_cardmarket = result_JSON.seven_day_prices_cardmarket
    let card_price_tcgplayer = result_JSON.seven_day_prices_tcgplayer
    let card_price_ebay = result_JSON.seven_day_prices_ebay
    let card_price_amazon = result_JSON.seven_day_prices_amazon
    let card_price_coolstuffinc = result_JSON.seven_day_prices_coolstuffinc
    let card_comment = result_JSON.comment
    console.log('card_comment', card_comment)
    console.log("CARD DETAILS :", card_details)
    console.log("CARD_PRICES :", card_prices)

    // change null to NAN
    for (let card_detail in card_details) {
        if (card_details[card_detail] == null) {
            // console.log(card_detail,"is null")
            card_details[card_detail] = "---"
        }
    }

    let cardDetails_HTML =
        /*html*/
        `
      <!--------------------------------  Title -------------------------------->
  <div class="uk-section">
  <div class="uk-container uk-content-center">
    <ul class="uk-breadcrumb ">
      <li><a href="/">Home</a></li>
      <li><a href="/card-market.html?page=1">Card Market</a></li>
      <li><span>${card_details.name}</span></li>
    </ul>
    <h3>${card_details.name}</h3>
    <p class="uk-article-meta">${card_details.card_sets_name}</p>
    <!--------------------------------  Title -------------------------------->

    <!--------------------------------  Tab Area -------------------------------->
    <div class="uk-grid-match">
      <div class="uk-position-relative uk-margin-medium">
        <ul class="uk-tab uk-flex-center uk-margin-remove-bottom tab-margin-setting" uk-tab="swiping: false">
          <li id="tab-bar-position">
            <a href="#">Info.</a>
          </li>
          <li>
            <a href="#">Comments</a>
          </li>
        </ul>
        <ul class="uk-switcher uk-margin uk-section-muted">
          <!--------------------------------  Cards info Area Start -------------------------------->
          <li>
            <div class="uk-grid-match uk-child-width-expand@m " uk-grid>
              <div class="card-detail-padding">
                <img src="${card_details.image_url}" class="card-detail-image-size" alt="">
              </div>
              <div id="card-detail-data">
                <div id="card_details-${card_details.name}">Name: ${card_details.name}</div>
                <div>Level : ${card_details.level}</div>
                <div>Number: ${card_details.api_id}</div>
                <div>Attribute: ${card_details.attribute}</div>
                <div>Card Type: ${card_details.type}</div>
                <div>Race : ${card_details.race}</div>
                <div>Archetype: ${card_details.archetype}</div>
                <div>ATK : ${card_details.atk}</div>
                <div>DEF: ${card_details.def}</div>
                <div>Content: ${card_details.desc}
                </div>
              </div>
              <!---------------------------------canvas chart Start---------------------------->
              <div class="chart-new-container uk-card uk-card-body ">
                <canvas id="myChart"></canvas>
                <!----------------------------------canvas chart End-------------------------------->
                <!----------------------------------Cart Area Start-------------------------------->
                <form action="">
                  <div class="uk-card uk-card-default uk-card-hover uk-card-body shopping-cart-style" uk-grid>
                  <div id="price-size-style">$ 999.99</div>
                    <div id="price-near-style">shipping: included</div>
                    <span id="price-seller-style">Sold by seller1</span>
                    <div class="uk-margin-remove-top price-select-style">
                      <span>
                        <select class="uk-select uk-card-body select-bar-style">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                          <option>6</option>
                          <option>7</option>
                          <option>8</option>
                        </select>
                      <button type="button" class="uk-button uk-button-primary uk-button-large cart-radius-style">
                        Add to Cart
                      </button>
                      </span>
                    </div>
                  </div>
                </form>
                <!----------------------------------Cart Area End-------------------------------->
              </div>
              <!----------------------------------Seller list Area start-------------------------------->
            </div>
            <table class="uk-table uk-table-hover uk-table-divider seller-list-style">
              <thead>
              <tr class="uk-text-center">
                <th>Seller</th>
                <th>Last Updated</th>
                <th>Offer</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>${card_prices[0].card_market}</td>
                <td>${card_prices[0].card_prices_created_at}</td>
                <td>${card_prices[0].price}</td>
              </tr>
              <tr>
                <td>${card_prices[1].card_market}</td>
                <td>${card_prices[1].card_prices_created_at}</td>
                <td>${card_prices[1].price}</td>
              </tr>
              <tr>
                <td>${card_prices[2].card_market}</td>
                <td>${card_prices[2].card_prices_created_at}</td>
                <td>${card_prices[2].price}</td>
              </tr>
              <tr>
                <td>${card_prices[3].card_market}</td>
                <td>${card_prices[3].card_prices_created_at}</td>
                <td>${card_prices[3].price}</td>
              </tr>
              <tr>
                <td>${card_prices[4].card_market}</td>
                <td>${card_prices[4].card_prices_created_at}</td>
                <td>${card_prices[4].price}</td>
              </tr>
              </tbody>
            </table>
            <!----------------------------------Seller list Area End-------------------------------->
          </li>
          <!--------------------------------  Cards info Area End -------------------------------->

          <!--------------------------------  Comments Area Start -------------------------------->
          <li>
            <div class="card-detail-padding">
              <img src="${card_details.image_url}" class="card-detail-image-size" alt="">
            </div>

            <ul class="uk-comment-list">
                  <li>
                    <article class="uk-comment uk-comment-primary uk-visible-toggle" tabindex="-1">
                      <header class="uk-comment-header uk-position-relative">
                        <div class="uk-grid-medium uk-flex-middle" uk-grid>
                          <div class="uk-width-auto">
                            <img class="uk-comment-avatar avatar-size-style" src="/test-avatar.jpeg" alt="">
                          </div>
                          <div class="uk-width-expand">
                            <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Author</a></h4>
                            <p class="uk-comment-meta uk-margin-remove-top"><a class="uk-link-reset" href="#">12 days ago</a></p>
                          </div>
                        </div>
                        <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted" href="#">Reply</a></div>
                      </header>
                      <div class="uk-comment-body">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                      </div>
                    </article>
                  </li>
            </ul>
            <form id="comment-form">
              <fieldset class="uk-fieldset">
                <legend class="uk-legend"></legend>
                <div class="uk-margin">
                  <input class="uk-input" type="text" placeholder="Input" name="comment">
                  <input type="submit" value="submit" />
                </div>
              </fieldset>
            </form>
          </li>
        </ul>
        <!--------------------------------  Comments Area End -------------------------------->

        <!--------------------------------  Tab Area -------------------------------->
      </div>
    </div>
  </div>
</div>
</main>
    `
    cardDetailsContainerElem.innerHTML = cardDetails_HTML
        // document.getElementById(`card_details-${card_details.name}`).innerText = card_details.name;

    // fetch Comment
    let cardCommentContainerElem = document.querySelector('.uk-comment-list')
    cardCommentContainerElem.innerHTML = ""

    if (card_comment.length !== 0) {
        for (let i = 0; i < card_comment.length; i++) {
            cardComment_HTML =
                /*html*/
                `
          <li>
          <article class="uk-comment uk-comment-primary uk-visible-toggle" tabindex="-1">
            <header class="uk-comment-header uk-position-relative">
              <div class="uk-grid-medium uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <img class="uk-comment-avatar avatar-size-style" src="/test-avatar.jpeg" alt="">
                </div>
                <div class="uk-width-expand">
                  <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Author</a></h4>
                </div>
              </div>
              <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted" href="#">Reply</a></div>
            </header>
            <div class="uk-comment-body">
              <p>${card_comment[i].comment}</p>
            </div>
          </article>
        </li>
      `
            cardCommentContainerElem.innerHTML += cardComment_HTML
        }
    } else {
        cardComment_HTML =
            /*html*/
            `
  <li>
  <article class="uk-comment uk-comment-primary uk-visible-toggle" tabindex="-1">
      <p>No Comment yet....</p>
  </article>
</li>
`
        cardCommentContainerElem.innerHTML = cardComment_HTML
    }
    // Line Chart Area

    const ctx = document.getElementById('myChart').getContext('2d');
    const skipped = (ctx, value) => ctx.p0.skip || ctx.p1.skip ? value : undefined;
    const down = (ctx, value) => ctx.p0.parsed.y > ctx.p1.parsed.y ? value : undefined;
    let labels = []
    let data_cardmarket = []
    let data_tcgplayer = []
    let data_ebay = []
    let data_amazon = []
    let data_coolstuffinc = []

    for (let card_data of card_price_cardmarket) {
        labels.push(`${card_data.card_prices_created_at}`.substring(0, 10))
        data_cardmarket.push(`${card_data.price}`)
    }
    for (let card_data of card_price_tcgplayer) {
        data_tcgplayer.push(`${card_data.price}`)
    }
    for (let card_data of card_price_ebay) {
        data_ebay.push(`${card_data.price}`)
    }
    for (let card_data of card_price_amazon) {
        data_amazon.push(`${card_data.price}`)
    }
    for (let card_data of card_price_coolstuffinc) {
        data_coolstuffinc.push(`${card_data.price}`)
    }

    const genericOptions = {
        fill: false,
        interaction: {
            intersect: false
        },
        radius: 0,
    };

    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Card Market',
                backgroundColor: 'rgb(100, 150, 100)',
                data: data_cardmarket,
                borderColor: 'rgb(100, 150, 100)',
                segment: {
                    borderColor: ctx => skipped(ctx, 'rgb(0,0,0,0.2)') || down(ctx, 'rgb(192,75,75)'),
                    borderDash: ctx => skipped(ctx, [6, 6]),
                },
                spanGaps: true
            }, {
                label: 'TCG player',
                backgroundColor: 'rgb(150, 100, 150)',
                data: data_tcgplayer,
                borderColor: 'rgb(150, 100, 150)',
                segment: {
                    borderColor: ctx => skipped(ctx, 'rgb(0,0,0,0.2)') || down(ctx, 'rgb(192,75,75)'),
                    borderDash: ctx => skipped(ctx, [6, 6]),
                },
                spanGaps: true
            }, {
                label: 'eBay',
                backgroundColor: 'rgb(150, 100, 100)',
                data: data_ebay,
                borderColor: 'rgb(150, 100, 100)',
                segment: {
                    borderColor: ctx => skipped(ctx, 'rgb(0,0,0,0.2)') || down(ctx, 'rgb(192,75,75)'),
                    borderDash: ctx => skipped(ctx, [6, 6]),
                },
                spanGaps: true
            }, {
                label: 'Amazon',
                backgroundColor: 'rgb(100, 100, 150)',
                data: data_amazon,
                borderColor: 'rgb(100, 100, 150)',
                segment: {
                    borderColor: ctx => skipped(ctx, 'rgb(0,0,0,0.2)') || down(ctx, 'rgb(192,75,75)'),
                    borderDash: ctx => skipped(ctx, [6, 6]),
                },
                spanGaps: true
            }, {
                label: 'Cool Stuffinc',
                backgroundColor: 'rgb(100, 100, 100)',
                data: data_coolstuffinc,
                borderColor: 'rgb(100, 100, 100)',
                segment: {
                    borderColor: ctx => skipped(ctx, 'rgb(0,0,0,0.2)') || down(ctx, 'rgb(192,75,75)'),
                    borderDash: ctx => skipped(ctx, [6, 6]),
                },
                spanGaps: true
            }]
        },
        options: {
            genericOptions,
            responsive: true,
            maintainAspectRatio: false
        }
    });
    // Line Area End

}


// Comment

const socket = io.connect()

//join room
let socketCardId = selectedSocketCardId
socket.emit("join-room", socketCardId)

async function fetchComment() {
    let commentForm = document.querySelector('#comment-form')
    commentForm.addEventListener('submit', async function(event) {
        event.preventDefault()

        const form = event.target
        const formObject = {}
        formObject['comment'] = form.comment.value
        formObject['socketCardId'] = socketCardId

        console.log('formObject =', formObject)
        socket.emit('part_A', formObject)

        let cardCommentContainerElem = document.querySelector('.uk-comment-list')

        cardComment_HTML =
            /*html*/
            `
    <li>
    <article class="uk-comment uk-comment-primary uk-visible-toggle" tabindex="-1">
      <header class="uk-comment-header uk-position-relative">
        <div class="uk-grid-medium uk-flex-middle" uk-grid>
          <div class="uk-width-auto">
            <img class="uk-comment-avatar avatar-size-style" src="/test-avatar.jpeg" alt="">
          </div>
          <div class="uk-width-expand">
            <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Author</a></h4>
          </div>
        </div>
        <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted" href="#">Reply</a></div>
      </header>
      <div class="uk-comment-body">
        <p>${formObject.comment}</p>
      </div>
    </article>
  </li>
  `
        cardCommentContainerElem.innerHTML += cardComment_HTML

    })
}

socket.on('part_B', (formObject) => {
    console.log('formObject comment = ', formObject.comment)
        //html ++
    let cardCommentContainerElem = document.querySelector('.uk-comment-list')

    cardComment_HTML =
        /*html*/
        `
  <li>
  <article class="uk-comment uk-comment-primary uk-visible-toggle" tabindex="-1">
    <header class="uk-comment-header uk-position-relative">
      <div class="uk-grid-medium uk-flex-middle" uk-grid>
        <div class="uk-width-auto">
          <img class="uk-comment-avatar avatar-size-style" src="/test-avatar.jpeg" alt="">
        </div>
        <div class="uk-width-expand">
          <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Author</a></h4>
        </div>
      </div>
      <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted" href="#">Reply</a></div>
    </header>
    <div class="uk-comment-body">
      <p>${formObject.comment}</p>
    </div>
  </article>
</li>
`
    cardCommentContainerElem.innerHTML += cardComment_HTML

})

// fetch User
async function fetchUser() {
    let res = await fetch("/user");
    if (res.ok) {
        const result = await res.json();
        return result;
    } else {
        return 'fetch user is down';
    }
}

async function login() {
    let user = await fetchUser()
    console.log('user from index', user);
    if (user.info != null) {
        const { id, username, avatar } = user
        console.log(id, username, avatar)
        document.querySelector(".sign-in-line-style").innerHTML = `
      <a class="uk-button uk-button-default loginColor" href="#" type="button" id="dropdownMenuLink">
                  <img src="/test-avatar.jpeg" class="avatar" alt="Avatar">
                  <span>${user.info.username}</span>
      </a>
      
      <div uk-dropdown="pos: bottom-justify">
                  <ul class="uk-nav uk-dropdown-nav">
                    <li><a class="uk-active" href="#">Profile</a></li>
                    <li><a class="uk-active" href="#">Cart</a></li>
                    <li><a class="uk-active" onclick="logout()" href="index.html">Log Out</a></li>
                  </ul>
      </div>
    `
    } else {
        document.querySelector('.sign-in-line-style').innerHTML = `
    <a class="sign-in-line-style non-logIn-style log-in-decoration" href="/sign-in.html">
                    Sign In
                  </a>
    `
    }
}

async function logout() {

    await fetch('/user/logout', { method: 'get' })

}

async function initCardDetails() {
    login()
    await fetchCardDetails()
    await fetchComment()
}

async function getMe() {
    let res = await fetch('/me')
    let result = await res.json()
    return result
}

initCardDetails()