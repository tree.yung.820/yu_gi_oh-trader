import { Knex } from "knex";
import { camelCaseKeys } from "../util/helper";
import fetch from "node-fetch";

export class UserService {
	constructor(private knex: Knex) {}

	async getUserByUsername(username: string) {
		return (
			await this.knex("users")
				.select("id", "username", "email", "password", "avatar", "role")
				.where("username", username)
		)[0];
	}

	async getUserByEmail(email: string) {
		return (
			await this.knex("users")
				.select("id", "username", "email", "password", "avatar", "role")
				.where("email", email)
		)[0];
	}

	async addUser(username: string, password: string, email: string, avatar: string | null) {
		return (
			await this.knex("users")
				.insert({ username, password, email, avatar })
				.returning(["id", "username", "email", "avatar", "role"])
		)[0];
	}

	async getGoogleInfo(accessToken: string) {
		const res = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
			method: "get",
			headers: {
				Authorization: `Bearer ${accessToken}`,
			},
		});

		const result = await res.json();
		return result;
	}

	async updateSetting(username: string, hashedPassword: any, filename: any, userId: number) {
		console.log("username", username);

		return camelCaseKeys(
			await this.knex.raw(
				/*sql*/
				`update users set username = COALESCE(?, username),
				password = COALESCE(?, password),
				avatar = COALESCE(?, avatar)
				where id = ?;`,
				[username, hashedPassword, filename, userId]
			)
		);
	}
}
