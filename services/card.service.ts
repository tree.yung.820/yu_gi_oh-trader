import { Knex } from "knex";
import fetch from "node-fetch";

export class CardService {
	constructor(private knex: Knex) {}

	async getCardSanic(body: any) {
		let result = await fetch("http://0.0.0.0:8000/scan-card", {
			method: "POST",
			body: body,
		});
		let res = await result;
		return res;
	}

	async getCardInfoByApiId(api_id: { classname: string; confidence: string }) {
		let classname = api_id.classname;
		return await this.knex("cards").where("api_id", classname);
	}

	async getIndexCardsDetails() {
		let card_market = "amazon_price";

		let result_NLC = (
			await this.knex.raw(
				`
		SELECT 
		cards.id as card_id,
		cards.name,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at,
		card_images.image_url
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		INNER JOIN card_images on card_images.card_id = cards.id
		WHERE 
		card_market = ?
		ORDER BY created_at DESC, card_id DESC
		LIMIT 3 OFFSET 0;`,
				[card_market]
			)
		).rows;

		let result_TS = (
			await this.knex.raw(
				`SELECT 
		cards.id as card_id,
		cards.name,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at,
		card_images.image_url
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		INNER JOIN card_images on card_images.card_id = cards.id
		WHERE 
		card_market = ?
		ORDER BY created_at DESC,price DESC
		LIMIT 3;`,
				[card_market]
			)
		).rows;

		let result_RS = (
			await this.knex.raw(
				`SELECT 
		cards.id as card_id,
		cards.name,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at,
		card_images.image_url
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		INNER JOIN card_images on card_images.card_id = cards.id
		WHERE 
		card_market = ?
		ORDER BY created_at DESC,price DESC
		LIMIT 100 OFFSET 97;`,
				[card_market]
			)
		).rows;

		let result = await { data_NLC: result_NLC, data_TS: result_TS, data_RS: result_RS };
		return result;
	}

	async getCardMarketCardDetails(page: any) {
		let result = (
			await this.knex.raw(
				`SELECT 
		cards.id as card_id,
		cards.name,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at,
		card_images.image_url
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		INNER JOIN card_images on card_images.card_id = cards.id
		WHERE 
		card_market = 'amazon_price'
		ORDER BY created_at DESC, card_id ASC
		LIMIT ? OFFSET ?;`,
				[16, (page - 1) * 16]
			)
		).rows;

		return result;
	}

	async getCardDetails(cardId: any) {
		let result_card_details = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		cards.name,
		cards.level,
		cards.api_id,
		cards.attribute,
		cards.type,
		cards.race,
		cards.archetype,
		cards.atk,
		cards.def,
		cards.desc,
		card_images.image_url,
		card_sets.name as card_sets_name
		FROM cards
		INNER JOIN card_images on card_images.card_id = cards.id
		FULL OUTER JOIN card_sets on card_sets.card_id = cards.id
		where cards.id = ?
		`,
				[cardId]
			)
		).rows;

		let result_card_prices = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		where cards.id = ?
		ORDER BY card_prices_created_at DESC
		`,
				[cardId]
			)
		).rows;

		let seven_day_prices_cardmarket = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		where cards.id = ? AND card_prices.card_market = 'cardmarket_price'
		ORDER BY card_prices_created_at ASC
		LIMIT 7
		`,
				[cardId]
			)
		).rows;

		let seven_day_prices_tcgplayer = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		where cards.id = ? AND card_prices.card_market = 'tcgplayer_price'
		ORDER BY card_prices_created_at ASC
		LIMIT 7
		`,
				[cardId]
			)
		).rows;

		let seven_day_prices_ebay = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		where cards.id = ? AND card_prices.card_market = 'ebay_price'
		ORDER BY card_prices_created_at ASC
		LIMIT 7
		`,
				[cardId]
			)
		).rows;

		let seven_day_prices_amazon = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		where cards.id = ? AND card_prices.card_market = 'amazon_price'
		ORDER BY card_prices_created_at ASC
		LIMIT 7
		`,
				[cardId]
			)
		).rows;

		let seven_day_prices_coolstuffinc = (
			await this.knex.raw(
				`
		SELECT
		cards.id as cards_id,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		where cards.id = ? AND card_prices.card_market = 'coolstuffinc_price'
		ORDER BY card_prices_created_at ASC
		LIMIT 7
		`,
				[cardId]
			)
		).rows;

		let comment = (
			await this.knex.raw(
				`
			SELECT
			cards.id as cards_id,
			comment,
			comments.created_at as comment_created_at
			FROM cards
			INNER JOIN comments on comments.card_id = cards.id
			where cards.id = ?
			ORDER BY comment_created_at ASC
			`,
				[cardId]
			)
		).rows;

		let result = {
			result_card_details,
			result_card_prices,
			seven_day_prices_cardmarket,
			seven_day_prices_tcgplayer,
			seven_day_prices_ebay,
			seven_day_prices_amazon,
			seven_day_prices_coolstuffinc,
			comment,
		};
		return result;
	}

	async getSearchCards(searchName: any) {
		let result = (
			await this.knex.raw(
				`SELECT 
		cards.id as card_id,
		cards.name,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at,
		card_images.image_url
		FROM cards
		INNER JOIN card_prices on card_prices.card_id = cards.id
		INNER JOIN card_images on card_images.card_id = cards.id
		WHERE 
		card_prices.created_at>CURRENT_DATE - INTERVAL'12 hours' AND
		card_market = 'amazon_price' AND
		UPPER(cards.name) like UPPER(?)
		ORDER BY created_at DESC, card_id ASC
		LIMIT 100 OFFSET 0;`,
				[`%${searchName}%`]
			)
		).rows;

		return result;
	}

	async getFilterResult(filterElem: any) {
		let card_type = filterElem.selectCardType;
		let card_race = filterElem.selectRace;
		let card_attribute = filterElem.selectAttribute;
		let card_archetype = filterElem.selectCardArchetype;
		// let card_format = filterElem.selectFormat
		let card_star = filterElem.selectStar;
		let card_atk = filterElem.cardAtk;
		let cark_def = filterElem.cardDef;
		let card_linkValue = filterElem.linkValue;
		let card_scaleValue = filterElem.scaleValue;
		let card_price = filterElem.cardPrice;

		console.log("filterElem :", filterElem);
		let result = await this.knex
			.select(
				this.knex.raw(`
		cards.id as card_id,
		cards.created_at as cards_created_at,
		cards.*,
		card_prices.price,
		card_prices.card_market,
		card_prices.created_at as card_prices_created_at,
		card_images.image_url
		`)
			)
			.from("cards")
			.join(this.knex.raw("card_prices on card_prices.card_id = cards.id"))
			.join(this.knex.raw("card_images on card_images.card_id = cards.id"))
			// .whereRaw("card_prices.created_at > CURRENT_DATE - INTERVAL'12 hours'")
			.where(function () {
				if (card_type !== "Select Card Type") {
					this.andWhere("type", card_type);
				}
				if (card_race !== "Select Race") {
					this.andWhere("race", card_race);
				}
				if (card_attribute !== "Select Attribute") {
					this.andWhere("attribute", card_attribute);
				}
				if (card_archetype !== "Select Card Archetype") {
					this.andWhere("archetype", card_archetype);
				}
				// if(card_format !== 'Select Format'){
				// 	this.andWhere('format', card_format)
				// }
				if (card_star !== "Select star") {
					this.andWhere("level", card_star);
				}
				if (card_atk !== "") {
					this.andWhere("atk", card_atk);
				}
				if (cark_def !== "") {
					this.andWhere("def", cark_def);
				}
				if (card_linkValue !== "") {
					this.andWhere("linkval", card_linkValue);
				}
				if (card_scaleValue !== "") {
					this.andWhere("scale", card_scaleValue);
				}
				if (card_price !== "1") {
					this.andWhere("price", "<", card_price);
				}
			})
			.andWhere("card_market", "amazon_price")
			.orderByRaw("card_prices.created_at DESC, card_id ASC")
			.limit(100);

		console.log("result :", result);

		return result;
	}
}
