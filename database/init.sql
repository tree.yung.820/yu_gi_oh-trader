-------------STEPs----------------

-- 1. CREATE DB in psql
psql
CREATE DATABASE yu_gi_oh_trader

-- 2. Change ./env
DB_NAME=yu_gi_oh_trader
DB_USERNAME=(your username)
DB_PASSWORD=(your password)
POSTGRES_DB_NAME=yu_gi_oh_trader_test
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_HOST=
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
PORT=8080
SECRET=yu_gi_oh_trader
NODE_ENV=


-- 3. yarn installl
yarn install


-- 4. knex command
yarn knex migrate:latest
yarn knex seed:run

if nescessary >>> yarn knex migrate:rollback <<<


-- 5. incert daily price
cd database 
npx ts-node fetch_price.ts
