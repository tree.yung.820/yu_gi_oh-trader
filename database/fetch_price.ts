import fetch from "node-fetch"
import dotenv from "dotenv";
import { knex } from "../util/db";
// import cron from 'cron'

// -----Auto daily run-----
// let CronJob = cron.CronJob;
// new CronJob(
// 	'* * * * *',
// 	getJson,
// 	()=>{
//         console.log('Job success')
//         console.table(resultList)
//     },
// 	true,
// 	'Asia/Hong_Kong'
// );

dotenv.config();

let data: Array<any> = []

async function getJson() {
    const res = await fetch(
        'https://db.ygoprodeck.com/api/v7/cardinfo.php',
    )
    const result = await res.json()
    data = result.data
}

async function importPrice() {
    console.log("Price fetching......")
    const txn = await knex.transaction();
    try {
        for (let eachCard of data) {
            let each_card_name = eachCard.name
            let card_id = await txn.select("id").where("name", each_card_name).from("cards")
            // console.log("card_id[0]", card_id[0])
            if (card_id[0] !== undefined) {
                // console.log("card_id :", card_id)
                let card_prices_JSON = eachCard.card_prices
                // console.log("card_prices_JSON", card_prices_JSON)
                for (let card_price_market in card_prices_JSON[0]) {
                    let each_card_price_JSON = {
                        "card_id": card_id[0].id,
                        "card_market": card_price_market,
                        "price": card_prices_JSON[0][card_price_market]
                    }
                    await txn("card_prices").insert(each_card_price_JSON)
                }
            }
        }
        await txn.commit();
    } catch (error) {
        console.log(error)
        await txn.rollback();
    }
    console.log("Price fetched successfully!")
}

async function run() {
    await getJson()
    await importPrice()
    await knex.destroy()
}

run()