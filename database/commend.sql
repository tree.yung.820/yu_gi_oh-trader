-- select * from cards;

-- select * from card_sets;

-- select * from card_prices;
-- select * from card_prices where card_id = 1;

-- select * from card_images;

-- select * from users;

-- select * from user_history;

-- select * from commments;

-- SELECT DISTINCT race FROM cards;

-- SELECT DISTINCT archetype FROM cards;

-- SELECT cards.id as card_id,
-- cards.name,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_market = 'amazon_price'
-- ORDER BY created_at DESC,price DESC
-- LIMIT 3;

-- SELECT
-- cards.id as card_id,
-- cards.name,
-- cards.created_at as cards_created_at,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at as card_prices_created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_market = 'amazon_price'
-- ORDER BY card_id DESC, card_prices_created_at DESC
-- LIMIT 3;


-- SELECT 
-- cards.id as card_id,
-- cards.name,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_market = 'amazon_price'
-- ORDER BY created_at DESC, card_id DESC
-- LIMIT 16 OFFSET 0;

-- SELECT
-- cards.id as cards_id,
-- cards.name,
-- cards.level,
-- cards.api_id,
-- cards.attribute,
-- cards.type,
-- cards.race,
-- cards.archetype,
-- cards.atk,
-- cards.def,
-- cards.desc,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_images on card_images.card_id = cards.id
-- where cards.id = 1

-- SELECT
-- cards.id as cards_id,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at as card_prices_created_at
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- where cards.id = 1
-- ORDER BY card_prices_created_at DESC

-- SELECT
-- cards.id as cards_id,
-- cards.name,
-- cards.level,
-- cards.api_id,
-- cards.attribute,
-- cards.type,
-- cards.race,
-- cards.archetype,
-- cards.atk,
-- cards.def,
-- cards.desc,
-- card_images.image_url,
-- card_sets.name as card_sets_name
-- FROM cards
-- INNER JOIN card_images on card_images.card_id = cards.id
-- INNER JOIN card_sets on card_sets.card_id = cards.id
-- where cards.id = 1

-- SELECT
-- cards.id as cards_id,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at as card_prices_created_at
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- where cards.id = 4340 AND card_prices.card_market = 'ebay_price'
-- ORDER BY card_prices_created_at ASC
-- LIMIT 7

-- SELECT 
-- cards.id as card_id,
-- cards.name,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_prices.created_at>CURRENT_DATE - INTERVAL'12 hours' AND
-- cards.name like '%cell%' AND
-- card_market = 'amazon_price'
-- ORDER BY created_at DESC, card_id ASC
-- LIMIT 16 OFFSET 0

-- SELECT 
-- cards.id as card_id,
-- cards.name,
-- card_prices.price,
-- card_prices.card_market,
-- max(card_prices.created_at) as card_price_created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_market = 'amazon_price' AND
-- cards.name like '%cell%'
-- group by 
-- cards.id, 
-- card_prices.price, 
-- card_prices.card_market, 
-- card_Images.image_url
-- ORDER BY card_price_created_at DESC, card_id ASC
-- LIMIT 30 OFFSET 0

-- with latest_quotation as (

-- select id, card_market,card_id,  max(created_at) from card_prices
-- where card_market = 'amazon_price'
-- group by id
-- order by created_at DESC 
-- limit 16

-- )

-- select 
-- cards.id, 
-- cards.name,
-- card_images.card_id,
-- latest_quotation.* 
-- from latest_quotation 
-- inner join cards on card_id = cards.id
-- inner join card_images on card_images.card_id = cards.id
-- ;


-- SELECT 
-- cards.id as card_id,
-- cards.name,
-- card_prices.price,
-- card_prices.card_market,
-- max(card_prices.created_at) as card_price_created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_market = 'amazon_price' AND
-- cards.name like '%cell%'
-- group by 
-- cards.id, 
-- card_prices.price, 
-- card_prices.card_market, 
-- card_Images.image_url
-- ORDER BY card_price_created_at DESC, card_id ASC
-- LIMIT 16 OFFSET 0

-- SELECT 
-- cards.id as card_id,
-- cards.created_at as cards_created_at,
-- cards.*,
-- card_prices.price,
-- card_prices.card_market,
-- card_prices.created_at as card_prices_created_at,
-- card_images.image_url
-- FROM cards
-- INNER JOIN card_prices on card_prices.card_id = cards.id
-- INNER JOIN card_images on card_images.card_id = cards.id
-- WHERE 
-- card_prices.created_at > CURRENT_DATE - INTERVAL'12 hours' AND
-- cards.type = ? AND
-- cards.race = ? AND
-- cards.attribute = ? AND
-- cards.archetype = ? AND
-- cards.level = ? AND
-- cards.atk = ? AND
-- cards.def = ? AND
-- cards.linkval = ? AND
-- cards.scale = ? AND
-- cards.price = ? AND
-- card_market = 'amazon_price'
-- ORDER BY card_prices.created_at DESC, card_id ASC
-- LIMIT 16 OFFSET 0







-- SELECT
-- cards.id as cards_id,
-- comment,
-- comments.created_at as comment_created_at
-- FROM cards
-- INNER JOIN comments on comments.card_id = cards.id
-- where cards.id = 10
-- ORDER BY comment_created_at ASC

-- SELECT
-- cards.id as cards_id,
-- cards.name,
-- cards.level,
-- cards.api_id,
-- cards.attribute,
-- cards.type,
-- cards.race,
-- cards.archetype,
-- cards.atk,
-- cards.def,
-- cards.desc,
-- card_images.image_url,
-- card_sets.name as card_sets_name
-- FROM cards
-- JOIN card_images on card_images.card_id = cards.id
-- FULL OUTER JOIN card_sets on card_sets.card_id = cards.id
-- where cards.id = 10

-- SELECT
-- *
-- from cards
-- where cards.id = 10