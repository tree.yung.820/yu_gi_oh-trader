import { NextFunction, Request, Response } from "express";
import { Knex } from "knex";
import { UserController } from "../controllers/user.controller";
import { UserService } from "../services/user.service";
import { checkPassword } from "../util/hash";
import { User } from "../util/models";

// jest.mock("../service/user.service");
jest.mock("../util/hash");

describe("UserController", function () {
	let controller: UserController;
	let service: UserService;
	let req: Request;
	let res: Response;
	let next: NextFunction;
	let resStatusSpy: jest.SpyInstance;
	let resJsonSpy: jest.SpyInstance;
	let resRedirectSpy: jest.SpyInstance;

	beforeEach(function () {
		let info: User = {
			id: 1,
			username: "1",
			password: "123",
			email: "",
			avatar: "",
			role: "user",
		};
		service = new UserService({} as Knex);
		service.getUserByUsername = jest.fn((username) => Promise.resolve(info));
		service.getUserByEmail = jest.fn((email) => Promise.resolve(info));
		service.addUser = jest.fn((username, password, email) => Promise.resolve());
		jest.spyOn(service, "getGoogleInfo").mockImplementation(async (accessToken) => info);

		controller = new UserController(service);
		req = {
			body: {},
			session: {},
			grant: { response: { accessToken: 1 } },
		} as any as Request;
		res = {
			status: jest.fn(() => res),
			json: jest.fn(),
			redirect: jest.fn(),
		} as any as Response;
		resStatusSpy = jest.spyOn(res, "status");
		resJsonSpy = jest.spyOn(res, "json");
		resRedirectSpy = jest.spyOn(res, "redirect");
	});

	describe("get /user", () => {
		test("get with user logged in", async () => {
			req.session["user"] = { id: 1 };
			controller.get(req, res);
			expect(resJsonSpy).toBeCalled;
			expect(resJsonSpy).toBeCalledWith({ info: { id: 1 } });
		});

		test("get /user without user logged in", async () => {
			controller.get(req, res);
			expect(resJsonSpy).toBeCalled;
			expect(resJsonSpy).toBeCalledWith({ info: null });
		});
	});

	describe("post /user/register", () => {
		test.todo("register");
	});

	describe("put /user/update", () => {
		test.todo("updateUser");
	});

	describe("post /user/login", () => {
		test("login with username", async () => {
			(checkPassword as jest.Mock).mockReturnValue(true);
			req.body = { username: "1", password: "123" };
			await controller.login(req, res, next);
			expect(service.getUserByUsername).toBeCalled();
			expect(checkPassword).toBeCalledWith(req.body.password, "123");
			expect(resJsonSpy).toBeCalledWith({ message: "User 1 logged in." });
		});

		test("login with email", async () => {
			(checkPassword as jest.Mock).mockReturnValue(true);
			req.body = { username: "1@1.com", password: "123" };
			await controller.login(req, res, next);
			expect(service.getUserByEmail).toBeCalled();
			expect(checkPassword).toBeCalledWith("123", "123");
			expect(resJsonSpy).toBeCalledWith({ message: "User 1 logged in." });
		});

		test("throw error with invalid username", async () => {
			service.getUserByUsername = jest.fn(() => Promise.resolve(null));
			await controller.login(req, res, next);
			expect(resStatusSpy).toBeCalledWith(400);
			expect(res.json).toBeCalledWith("Invalid username or password.");
		});

		test("throw error with invalid password", async () => {
			(checkPassword as jest.Mock).mockReturnValue(false);
			await controller.login(req, res, next);
			expect(resStatusSpy).toBeCalledWith(400);
			expect(res.json).toBeCalledWith("Invalid username or password.");
		});
	});

	describe("POST /user/login/google", () => {
		test.todo("loginGoogle");
	});

	describe("POST /user/logout", () => {
		test("logout", () => {
			controller.logout(req, res);
			expect(req.session["user"]).toBeUndefined();
			expect(resRedirectSpy).toBeCalled();
		});
	});
});
