import { Request, Response } from "express";
import { CardService } from "../services/card.service";
import { HttpError } from "../util/models";
import { RestfulController } from "./restful.controller";
import { form } from "../util/formidable";
import fs from "fs";
import { Buffer } from "buffer";
import { IncomingMessage } from "http";
import fetch from "node-fetch";
// import path from 'path'

export class CardController extends RestfulController {
	constructor(private cardService: CardService) {
		super();
		this.router.post("/card/scanner", this.cardScannerAi);
		this.router.post("/card/scanner/canvas", this.canvasDecode);
		this.router.get("/indexPage", this.getIndexCards);
		this.router.get("/card-market/:page", this.getCardMarketCards);
		this.router.get("/card-detail/:id", this.getCardInfoById);
		this.router.get("/card-market/search/:searchName", this.searchName);
		this.router.post("/filter", this.filterCards);
	}

	canvasDecode = this.handleRequest(async (req, res) => {
		console.log("Image Received");
		const uploadDir = "uploads";
		// fs.mkdirSync(uploadDir, { recursive: true });
		// let [imageMeta, imageDataURL] = req.body.image;
		// // let buff = Buffer.from(imageDataURL, "base64");
		// // fs.writeFileSync(uploadDir, buff);
		// console.log(req.body.image_base64);
		// let filePath = `./${uploadDir}/camera_photo${Date.now()}.jpg`;
		// fs.stat;
		// let result: any = await this.cardService.getCardSanic(uploadDir);
		// let card = await result.json();
		// let cardRes = await this.cardService.getCardInfoByApiId(card.classname);
		// console.log(cardRes[0]);
		// return cardRes;

		let result = await fetch("http://0.0.0.0:8000/scan-card", {
			method: "POST",
			body: req.body.image_base64,
		});
		result = await result.json();
		return result;
		return "decode ok";
	});

	cardScannerAi = this.handleRequest(async (req: Request, res: Response) => {
		// let cardForm = await formParse(req);
		let cardForm = await PromiseFormParse(req);
		let result = await this.cardService.getCardSanic(cardForm);
		// let card: { api_id: string; confidence: string } = await result.json();
		// console.log(card);
		let cardRes = await this.cardService.getCardInfoByApiId(await result.json());
		console.log(cardRes);
		return cardRes[0];
	});

	getScannerCard = this.handleRequest(async (req: Request, res: Response) => {});

	getIndexCards = this.handleRequest(async (req: Request, res: Response) => {
		let result = await this.cardService.getIndexCardsDetails();

		return result;
	});

	getCardMarketCards = this.handleRequest(async (req: Request, res: Response) => {
		let page = req.params.page;
		console.log("page =", page);
		let result_pre = await this.cardService.getCardMarketCardDetails(page);
		let result = { data: result_pre };

		return result;
	});

	getCardInfoById = this.handleRequest(async (req: Request, res: Response) => {
		let cardId = req.params.id;
		// console.log("select card ID= ", cardId);

		let result = await this.cardService.getCardDetails(cardId);

		return result;
	});

	searchName = this.handleRequest(async (req: Request, res: Response) => {
		let searchName = req.params.searchName;
		if (searchName == "null") {
			searchName = "/NAN";
		}
		console.log("searchingName =", searchName);
		let result_pre = await this.cardService.getSearchCards(searchName);
		// console.log('search result',result_pre)
		let result = { data: result_pre };

		return result;
	});

	filterCards = this.handleRequest(async (req: Request, res: Response) => {
		let filterElem = req.body; //it is selected filter element
		// console.log('filterElem =', filterElem)
		let result_pre = await this.cardService.getFilterResult(filterElem);
		let result = { data: result_pre };

		return result;
	});
}

async function PromiseFormParse(request: IncomingMessage) {
	return new Promise<any>((resolve, reject) => {
		form.parse(request, async (err, fields, files) => {
			let file = Array.isArray(files) ? files[0] : files;
			let body = file.image.filepath;
			if (file == null) throw new HttpError(400, "no image received");
			resolve(body);
		});
	});
}
