import { Request, Response } from "express";
import { UserService } from "../services/user.service";
import { checkPassword, hashPassword } from "../util/hash";
import { logger } from "../util/logger";
import { HttpError, User } from "../util/models";
import { RestfulController } from "./restful.controller";
import crypto from "crypto";
import { validate } from "../util/helper";
import { isLoggedIn } from "../middleware/guard";
import { form } from "../util/formidable";

export class UserController extends RestfulController {
	constructor(private userService: UserService) {
		super();
		this.router.get("/user", this.get);
		this.router.post("/user/login", this.login);
		this.router.post("/user/register", this.register);
		this.router.get("/user/login/google", this.googleLogin);
		this.router.get("/user/logout", isLoggedIn, this.logout);
		this.router.put("/user/update", isLoggedIn, this.updateUser);
	}

	get = (req: Request, res: Response) => {
		// set user theme
		req.session && req.session["user"] ? res.json({ info: req.session["user"] }) : res.json({ info: null });
	};

	login = this.handleRequest(async (req: Request, res: Response) => {
		validate(req.body);

		let { username, password } = req.body;
		let foundUser: User;
		logger.debug("%o", req.body);

		/@/.test(username)
			? (foundUser = await this.userService.getUserByEmail(username))
			: (foundUser = await this.userService.getUserByUsername(username));

		if (!foundUser) throw new HttpError(400, "Invalid username or password.");
		logger.debug("check !found");

		let isPasswordValid = await checkPassword(password, foundUser.password!);
		if (!isPasswordValid) {
			logger.debug("check !password");

			throw new HttpError(400, "Invalid username or password.");
		}

		delete foundUser["password"];
		logger.info("%o", foundUser);

		req.session && (req.session["user"] = foundUser);

		return { message: `User ${foundUser.username} logged in.` };
	});

	register = this.handleRequest(async (req: Request, res: Response) => {
		let error = await this.validateInput(req);
		if (error) {
			throw error;
		}
		let { username, email, password, avatar } = req.body;
		password = await hashPassword(password.toString());

		let user: User = await this.userService.addUser(username, password, email, avatar);
		req.session && (req.session["user"] = user);
		return { message: `user ${user.username} registered` };
	});

	googleLogin = async (req: Request, res: Response) => {
		let accessToken = req.session?.["grant"].response.access_token;
		let googleUserInfo = await this.userService.getGoogleInfo(accessToken);
		let foundUser: User = await this.userService.getUserByEmail(googleUserInfo.email);
		if (!foundUser) {
			let googleAccount = {
				username: googleUserInfo.name.concat(Date.now()),
				password: await hashPassword(crypto.randomBytes(20).toString("hex")),
				email: googleUserInfo.email,
				avatar: googleUserInfo.picture,
			};
			foundUser = await this.userService.addUser(
				googleAccount.username,
				googleAccount.password,
				googleAccount.email,
				googleAccount.avatar
			);
		}
		req.session["user"] = foundUser;
		res.redirect("/index.html");
	};

	logout = (req: Request, res: Response) => {
		delete req.session["user"];
		res.redirect("/index.html");
	};

	updateUser = this.handleRequest(async (req: Request, res: Response) => {
		const userId = req.session["user"].id;
		form.parse(req, async (err, field, files) => {
			//check picture file exist or not
			let file = Array.isArray(files.image) ? files.image[0] : files.image;
			let image = file.newFilename;
			let fileName = Object.keys(files).length === 0 ? null : image;
			//check password and confirm password have same input
			if (field.password != field.confirmPassword) {
				logger.debug("check !password");
				throw new HttpError(400, "Invalid password.");
			}

			let hashedPassword = field.password ? await hashPassword(field.password as string) : null;

			await this.userService.updateSetting(field.username as string, hashedPassword, fileName, userId);
			return;
		});
	});

	async validateInput(req: Request) {
		validate(req.body);
		let { username, email, password, confirmPassword } = req.body;

		let whiteSpace = /^\s+/;
		if (username.match(whiteSpace) || email.match(whiteSpace) || password.match(whiteSpace)) {
			return new HttpError(400, "Cannot into space.");
		}

		if (username.match(/@/)) {
			return new HttpError(400, "Cannot use @ in username.");
		}
		if (password !== confirmPassword) {
			return new HttpError(400, "The passwords you entered do not match.");
		}

		let user = await this.userService.getUserByUsername(username);
		if (user) return new HttpError(400, "Username has been used.");
		let userEmail = await this.userService.getUserByEmail(email);
		if (userEmail) return new HttpError(400, "This email address is not available. Choose a different address.");
		return;
	}
}
