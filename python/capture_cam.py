import cv2 as cv
import os
import numpy as np
import sys
import pathlib

def web_cam():
    cap = cv.VideoCapture(1)
    cap.set(3, 1920)
    # if sys.platform == "win32":
    #     cap.open(0,cv.CAP_DSHOW)
    img_counter = 0
    while(True):
        ret, frame = cap.read()

        cv.imshow('web_cam', frame)
        k = cv.waitKey(1)
        if k%256 == 27:
        # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            img_name = "opencv_frame_{}.png".format(img_counter)
            # resize = resize_IMG(100,img)
            img = cv.imwrite(img_name, frame)
            print("{} written!".format(img_name))
            img_counter += 1
            return img_name
    cap.release()
    cv.destroyAllWindows()

def detect(img):
    abs_path = os.path.join(os.getcwd(),img)
    return abs_path

def resize_IMG(percentage, img):
    scale_percent = percentage       # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized_img = cv.resize(img, dim, interpolation = cv.INTER_AREA)
    new_resized_img = cv.resize(img, dim, interpolation = cv.INTER_AREA)
    return resized_img, new_resized_img

def image_process(image, method):#grayscale image
    gs_img = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
# blur = cv.GaussianBlur(gs_img,(5,5),0)
#blur image
    blur = cv.bilateralFilter(gs_img,7,90,90)
#edge image
    edge_img = cv.Canny(blur,25, 75)
    if method == False:
        ret,thresh = cv.threshold(edge_img,127,255,cv.THRESH_BINARY&cv.THRESH_OTSU)
        return thresh
    elif method == True:
        adaptive_thresh = cv.adaptiveThreshold(edge_img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY,11,2)
        # cv.imshow('threshold',adaptive_thresh)
        # cv.waitKey(0)
        return adaptive_thresh

def get_contour(threshold, image):
    pts=None
    pts_exist = False
    contours,hierarchy = cv.findContours(threshold,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key = cv.contourArea, reverse=True)[1:5]
    for cnt in contours:
        pts = None
        epsilon = 0.05*cv.arcLength(cnt, True)
        approx = cv.approxPolyDP(cnt, epsilon, True)
        if len(approx) ==4 and cv.contourArea(cnt)>5000:
            target = approx
            pts = target.reshape(4,2)
            rect = np.zeros((4,2), dtype = "float32")
            s= pts.sum(axis = 1)
            rect[0] = pts[np.argmin(s)]
            rect[2] = pts[np.argmax(s)]
            diff = np.diff(pts, axis = 1)
            rect[1] = pts[np.argmin(diff)]
            rect[3] = pts[np.argmax(diff)]
            contour_img = cv.drawContours(image,[target], -1, (0,255,0),3)
        else:
            rect = None
        return rect

def wrap(rect, image):
    if rect is None:
        img_out = image
    else:
        pts_src =  np.array(rect)
#ygo card size = 8.6cm x5.9cm -> 325px x 222px
        (tl, tr, br, bl) = pts_src
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        maxHeight = max(int(heightA), int(heightB))
        pts_dst = np.array([[0,0],[maxWidth - 1,0],[maxWidth -1,maxHeight - 1],[0,maxHeight - 1]])
        h,status = cv.findHomography(pts_src, pts_dst)
        img_out = cv.warpPerspective(image,h,(maxWidth,maxHeight))
    return img_out

def save_picture(folder, new_file, image):
    if os.path.exists(folder) is False:
        pathlib.Path(folder).mkdir(exist_ok=True)
    cv.imwrite(new_file,image)

def capture_show(image,contour_image,output_image):
    cv.imshow("warpped image", image)
    cv.imshow("warpped image", contour_image)
    cv.imshow("warpped image", output_image)
    cv.waitKey(0)

def get_train_set():
    data_dir = os.path.join(os.getcwd(),'small_train_set')
    data_dir = pathlib.Path(data_dir)
    image_count = len(list(data_dir.glob('*/*.jpg')))
    image_arr = list(data_dir.glob('*/*.jpg'))
    print(image_count)
    return image_arr

def crop_train_set():
    path_arr= get_train_set()
    for path in path_arr:
        path = str(path.as_posix())
        image = cv.imread(path)
        old_path = os.path.normpath(path).split(os.path.sep)
        new_folder = os.path.join(os.getcwd(),'cropped_data_set',old_path[7])
        new_path = os.path.join(os.getcwd(),'cropped_data_set',old_path[7],old_path[8])
        new_img = image.copy()
        process = image_process(image, True)
        rect, pts = get_contour(process,new_img)
        img_out = wrap(rect,image,pts)
        save_picture(new_folder,new_path,img_out)


def run_web_cam():
    capture = web_cam()
    path = detect(capture)
    img = cv.imread(path)
    resize, new_resized_img = resize_IMG(100,img)
    process = image_process(resize, False)
    rect,contour = get_contour(process, resize)
    result = wrap(rect, new_resized_img)
    capture_show(process,contour,result)

# run_web_cam()
# crop_train_set()