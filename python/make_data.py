import os
import pandas as pd
import shutil

print (os.getcwd())

csv_file = os.path.join(os.getcwd(),'test_grab_JSON\grab_all.csv')
# print(csv_file)
df = pd.read_csv(csv_file)
# print(df)

for data in df.id:
    img_path = os.path.join(os.getcwd(),f"card_pic\pic\{data}.jpg")
    sm_img_path = os.path.join(os.getcwd(),f"card_pic\small_pic\{data}.jpg")
    os.makedirs(os.path.join(os.getcwd(),f"training\{data}"), exist_ok=True)
    target = os.path.join(os.getcwd(),f"training\{data}")
    img_target = os.path.join(os.getcwd(),f"training\{data}\{data}.jpg")
    sm_target = os.path.join(os.getcwd(),f"training\{data}\{data}-sm.jpg")
    shutil.copy(sm_img_path,target)
    os.rename(img_target,sm_target)
    shutil.copy(img_path,target)
    
    # print('img: ',img_path)
    # print('sm_img: ',sm_img_path)
