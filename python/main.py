import pathlib
import time
from sanic import Sanic
from sanic.response import json as res_json
import tensorflow as tf
from sanic_cors import CORS
import cv2 as cv
import os
import setting
import numpy as np
import json
import capture_cam as cc

app = Sanic("Card-Ai")
CORS(app)

data_dir = "cropped_data_set"
data_dir = pathlib.Path(data_dir)
class_names=list(data_dir.glob('*/'))
folder = []
for path in class_names:
    arr = os.path.split(path)
    tail = arr[1]
    folder.append(tail)

card_class = []
class_json = json.load(open("class_name.json"))
for i in class_json:
    card_class.append(i)



IMAGE_HEIGHT=setting.IMAGE_HEIGHT
IMAGE_WIDTH=setting.IMAGE_WIDTH

UPLOAD_DIR="uploads"

def load():
    global model
    model = tf.keras.models.load_model(setting.LOAD_MODEL)

def predict_file(image_path:str)->str:
    print("image_path: ",image_path)
    return predict_image(image_crop(image_path))

def image_crop(file_path):
    image = cv.imread(file_path)
    if image is None:
        print("image is none")
        return None
    resize, new_resized_img = cc.resize_IMG(100,image)
    process = cc.image_process(resize, False)
    rect= cc.get_contour(process, resize)
    result = cc.wrap(rect, new_resized_img)
    print("image: ",image.shape)
    image = cv.cvtColor(result, cv.COLOR_BGR2RGB)
    width = 160
    height = 160
    dim = (width, height)
    resized = cv.resize(image, dim, interpolation=cv.INTER_AREA)
    print("resized: ",resized.shape)
    img_array = tf.keras.utils.img_to_array(resized)
    img_array = tf.expand_dims(img_array,0)
    return img_array

def predict_image(image_arr):
    print("image_arr: ",image_arr.shape)
    global model
    predictions = model.predict(image_arr)
    score = tf.nn.softmax(predictions)
    print("score argmax: ", np.argmax(score))
    print("This card most likely belongs to {} with a {:.2f} percent confidence."
    .format(card_class[np.argmax(score)], 100*np.max(score))
    )
    classname=card_class[np.argmax(score)]
    result=dict()
    result["classname"]=classname
    result["confidence"]=f"{np.max(score)}"
    print("result: ",result)
    return result

import base64
@app.post("/scan-card")
async def scan_card(request):
    # url = str(request.body,'utf-8')
    # print("url: ",url)
    # return res_json(predict_file(url))
    
    url_blob=request.body
    blob=base64.b64decode(url_blob)
    current_time=time.time()
    image_path=f"uploads/{current_time}.jpg"
    image_result = open(image_path, 'wb')
    if pathlib.Path("uploads").exists() is False:
        pathlib.Path("uploads").mkdir()
    image_result.write(blob)
    return res_json(predict_file(image_path))

if __name__ == "__main__":
    load()
    app.run(host="0.0.0.0", port=8000)
