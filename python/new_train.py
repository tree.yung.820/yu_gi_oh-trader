import numpy as np
import os
import tensorflow as tf
import pathlib
import setting
import matplotlib as plt

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

print('1: ', tf.config.list_physical_devices('GPU'))
print('2: ', tf.test.is_built_with_cuda)
print('3: ', tf.test.gpu_device_name())
print('4: ', tf.config.get_visible_devices())

data_dir = os.path.join(os.getcwd(),'cropped_data_set')
data_dir = pathlib.Path(data_dir)
image_count = len(list(data_dir.glob('*/*.jpg')))
print(image_count)

BATCH_SIZE=setting.BATCH_SIZE
IMAGE_HEIGHT=setting.IMAGE_HEIGHT
IMAGE_WIDTH=setting.IMAGE_WIDTH
AUTOTUNE = tf.data.AUTOTUNE
USE_GPU=False

class_names=list(data_dir.glob('*/'))


train_ds = tf.keras.utils.image_dataset_from_directory(
data_dir,
validation_split=0.2,
subset="training",
seed=123,
image_size=(IMAGE_HEIGHT,IMAGE_WIDTH),
batch_size=BATCH_SIZE
)

val_ds = tf.keras.utils.image_dataset_from_directory(
data_dir,
validation_split=0.2,
subset="validation",
seed=123,
image_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
batch_size=BATCH_SIZE
)

AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

normalization_layer = layers.Rescaling(1./255)

normalized_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
image_batch, labels_batch = next(iter(normalized_ds))
first_image = image_batch[0]
# Notice the pixel values are now in `[0,1]`.
print(np.min(first_image), np.max(first_image))

num_classes = len(class_names)


data_augmentation = tf.keras.Sequential(
    [
      tf.keras.layers.RandomFlip("horizontal_and_vertical",
                        input_shape=(IMAGE_HEIGHT,
                                    IMAGE_WIDTH,
                                    3)),
      tf.keras.layers.RandomBrightness(factor=0.2,value_range=[0.,1.]),
      tf.keras.layers.RandomContrast([0.,1.]),
      tf.keras.layers.RandomRotation(0.2),
      tf.keras.layers.RandomZoom(0.1),
      tf.keras.layers.RandomTranslation(0.05, 0.05)
    ]
  )
model = Sequential([
    # layers.Rescaling(1./255, input_shape=(IMAGE_HEIGHT, IMAGE_WIDTH, 3)),
    data_augmentation,
    layers.Conv2D(16, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(32, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(64, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dense(num_classes)
])

model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0003),
                loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=['accuracy'])

model.summary()

epochs=100
history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochs
)
model.save(setting.MODEL_NAME,save_format='h5')
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()