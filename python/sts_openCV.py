import cv2 as cv
import sys
import numpy as np
import pathlib
import os

data_dir = os.path.join(os.getcwd(),'small_train_set')
data_dir = pathlib.Path(data_dir)
image_count = len(list(data_dir.glob('*/*.jpg')))
image_arr = list(data_dir.glob('*/*.jpg'))




for path in image_arr:
    path_str = str(path.as_posix())
    old_path = os.path.normpath(path).split(os.path.sep)
    image = cv.imread(path_str)
    gs_img = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    blur = cv.bilateralFilter(gs_img,7,90,90)
    edge_img = cv.Canny(blur,25, 75)
    ret,thresh = cv.threshold(edge_img,127,255,3)
    contours,hierarchy = cv.findContours(thresh,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key = cv.contourArea, reverse=True)[:5]
    for cnt in contours:
        epsilon = 0.05*cv.arcLength(cnt, True)
        approx = cv.approxPolyDP(cnt, epsilon, True)
        if len(approx) ==4 and cv.contourArea(cnt)>5000:
            target = approx
            pts = target.reshape(4,2)
            rect = np.zeros((4,2), dtype = "float32")
            s= pts.sum(axis = 1)
            rect[0] = pts[np.argmin(s)]
            rect[2] = pts[np.argmax(s)]
            diff = np.diff(pts, axis = 1)
            rect[1] = pts[np.argmin(diff)]
            rect[3] = pts[np.argmax(diff)]
            contour_img = cv.drawContours(image,[target], -1, (0,255,0),3)
            pts_src =  np.array(rect)
            (tl, tr, br, bl) = pts_src
            widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
            widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
            heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
            heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
            maxWidth = max(int(widthA), int(widthB))
            maxHeight = max(int(heightA), int(heightB))
            pts_dst = np.array([[0,0],[maxWidth - 1,0],[maxWidth -1,maxHeight - 1],[0,maxHeight - 1]])
            h,status = cv.findHomography(pts_src, pts_dst)
            img_out = cv.warpPerspective(image,h,(maxWidth,maxHeight))
            new_path = os.path.join(os.getcwd(),'cropped_data_set',old_path[7],old_path[8])
            # cv.imwrite(new_path,img_out)
            if maxHeight>512:
                cv.imshow(new_path,img_out)
                cv.waitKey()