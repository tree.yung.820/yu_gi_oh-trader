import cv2 as cv
import os
import numpy as np
import matplotlib.pyplot as plt


#read image
def detect(img_name):
    abs_path = os.path.join(os.getcwd(),img_name)
    return abs_path

# #check if image is loaded, if not, exit program
# if img is None:
#     sys.exit("Could not read the image.")
# #show height, width, depth of the image
# (h, w, d) = img.shape

def resize_IMG(percentage, img):
    global width, height, resized_img, new_resized_img
    scale_percent = percentage       # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized_img = cv.resize(img, dim, interpolation = cv.INTER_AREA)
    new_resized_img = cv.resize(img, dim, interpolation = cv.INTER_AREA)


def image_process(image, method):#grayscale image
    gs_img = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
# blur = cv.GaussianBlur(gs_img,(5,5),0)
#blur image
    blur = cv.bilateralFilter(gs_img,7,90,90)
#edge image
    edge_img = cv.Canny(blur,25, 75)
    if method == False:
        ret,thresh = cv.threshold(edge_img,127,255,3)
    elif method == True:
        adaptive_thresh = cv.adaptiveThreshold(edge_img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY,11,2)

def get_contour(threshold, image):
    contours,hierarchy = cv.findContours(threshold,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key = cv.contourArea, reverse=True)[1:4]
    for cnt in contours:
        epsilon = 0.1*cv.arcLength(cnt, True)
        approx = cv.approxPolyDP(cnt, epsilon, True)
    # print(cnt)
        area = cv.contourArea(cnt)
        if len(approx) ==4 :
            target = approx
            pts = target.reshape(4,2)
        print(pts)
        rect = np.zeros((4,2), dtype = "float32")
        s= pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        contour_img = cv.drawContours(image,[target], -1, (0,255,0),3)
# cv.waitKey(0)


#ygo card size = 8.6cm x5.9cm -> 325px x 222px
def wrap(rect, image):
    global img_out
    pts_src =  np.array(rect)

    pts_dst = np.array([[0,0],[width - 1,0],[width -1,height - 1],[0,height - 1]])

    h,status = cv.findHomography(pts_src, pts_dst)
    img_out = cv.warpPerspective(image,h,(image.shape[1],image.shape[0]))

def run_process(img):
    resize = resize_IMG(100,img)
    process = image_process(resize,True)
    contour = get_contour(process,resize)
    wrap(contour,resize)

cap = cv.VideoCapture(0)
cap.open(0,cv.CAP_DSHOW)
img_counter = 0
while(True):
    ret, frame = cap.read()
    cv.imshow('frame', frame)
    k = cv.waitKey(1)
    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img_name = "opencv_frame_{}.png".format(img_counter)
        # resize = resize_IMG(100,img)
        cv.imwrite(img_name, frame)
        path = detect(img_name)
        print("{} written!".format(img_name))
        img = cv.imread(path)
        run_process(img)
        img_counter += 1
cap.release()
cv.destroyAllWindows()
# cv.imshow("source image", new_resized_img)
# cv.imshow("contour image", contour_img)
cv.imshow("warpped image", img_out)

cv.waitKey(0)



# images_arr = [blur,gs_img,adaptive_thresh,cnt_draw]
# titles_arr = ['image', 'grayscale','adaptive_threshold', 'cnt_draw']
# for i in range(4):
#     plt.subplot(2,2,i+1),plt.imshow(images_arr[i],'gray')
#     plt.title(titles_arr[i])
#     plt.xticks([]),plt.yticks([])
# plt.show()

#show image,draw rectangle in between x=48,y=108 and x=374,y=436
# cv.rectangle(img,(48,108),(374,436),(0,255,0),3)
# cv.imshow('cam_pic', img)
# #keep the preview window open
# k = cv.waitKey(0)
# #save image if "s" key on the keyboard is pressed
# if k== ord("s"):
#     cv.imwrite("new.jpg", img)

# #show the rgb value of the image in point x=48, y=108
# #if "c" key on the keyboard is pressed
# if k== ord("c"):
#     (B, G, R) = img[108,48]
#     print("R={}, G={}, B={}".format(R, G, B))