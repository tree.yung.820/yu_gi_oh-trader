import { Express } from "express";
import { knex } from "./util/db";
import { UserController } from "./controllers/user.controller";
import { UserService } from "./services/user.service";
import { CardController } from "./controllers/card.controller";
import { CardService } from "./services/card.service";
import SocketIO from "socket.io";

export function attachApi(app: Express, io: SocketIO.Server) {
	let userService = new UserService(knex);
	let userController = new UserController(userService);
	let cardService = new CardService(knex);
	let cardController = new CardController(cardService);

	//let commentService = new CommentService(knex)
	//let commentController = new CommentController(commentService)

	// add socketIO route
	app.use(userController.router);
	app.use(cardController.router);
	// app.use(commentsController.route)
}
