import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	if (!(await knex.schema.hasTable("cards"))) {
		await knex.schema.createTable("cards", (table) => {
			table.increments();
			table.integer("api_id").notNullable();
			table.string("name").notNullable();
			table.string("type").notNullable();
			table.text("desc").notNullable();
			table.integer("atk").nullable();
			table.integer("def").nullable();
			table.string("banlist").nullable();
			table.integer("level").nullable();
			table.string("race").notNullable();
			table.string("attribute").nullable();
			table.integer("linkval").nullable();
			table.string("linkmarkers").nullable();
			table.string("archetype").nullable();
			table.integer("scale").nullable();
			table.timestamps(false, true);
		});
	}
	if (!(await knex.schema.hasTable("card_sets"))) {
		await knex.schema.createTable("card_sets", (table) => {
			table.increments();
			table.integer("card_id").unsigned();
			table.foreign("card_id").references("cards.id");
			table.string("name").notNullable();
			table.string("code").notNullable();
			table.string("rarity").notNullable();
			table.float("price").notNullable();
		});
	}
	if (!(await knex.schema.hasTable("card_prices"))) {
		await knex.schema.createTable("card_prices", (table) => {
			table.increments();
			table.integer("card_id").unsigned();
			table.foreign("card_id").references("cards.id");
			table.string("card_market").notNullable();
			table.float("price").notNullable();
			table.timestamps(false, true);
		});
	}
	if (!(await knex.schema.hasTable("card_images"))) {
		await knex.schema.createTable("card_images", (table) => {
			table.increments();
			table.integer("card_id").unsigned();
			table.foreign("card_id").references("cards.id");
			table.string("image_url").notNullable();
			table.string("small_pic").notNullable();
		});
	}
	if (!(await knex.schema.hasTable("users"))) {
		await knex.schema.createTable("users", (table) => {
			table.increments();
			table.string("username").notNullable();
			table.string("password").notNullable();
			table.string("email").notNullable();
			table.string("role").notNullable();
			table.text("avatar");
			table.timestamps(false, true);
		});
	}
	if (!(await knex.schema.hasTable("user_history"))) {
		await knex.schema.createTable("user_history", (table) => {
			table.increments();
			table.integer("user_id").unsigned();
			table.foreign("user_id").references("users.id");
			table.integer("card_id").unsigned();
			table.foreign("card_id").references("cards.id");
			table.float("price").notNullable();
			table.integer("buyer_id");
			table.integer("seller_id");
			table.timestamps(false, true);
		});
	}
	if (!(await knex.schema.hasTable("comments"))) {
		await knex.schema.createTable("comments", (table) => {
			table.increments();
			table.integer("user_id").unsigned();
			table.foreign("user_id").references("users.id");
			table.integer("card_id").unsigned();
			table.foreign("card_id").references("cards.id");
			table.text("comment").notNullable();
			table.timestamps(false, true);
		});
	}
	if (await knex.schema.hasTable("users")) {
		await knex.schema.alterTable("users", (t) => {
			t.string("avatar").defaultTo("test-avatar.jpeg").alter();
			t.string("role").defaultTo("user").alter();
		});
	}
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTableIfExists("comments");
	await knex.schema.dropTableIfExists("user_history");
	await knex.schema.dropTableIfExists("users");
	await knex.schema.dropTableIfExists("card_images");
	await knex.schema.dropTableIfExists("card_prices");
	await knex.schema.dropTableIfExists("card_sets");
	await knex.schema.dropTableIfExists("cards");
}
