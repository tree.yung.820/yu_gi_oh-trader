// import KnexInit from "knex";
import { Knex } from "knex";
import dotenv from "dotenv";
import XLSX from "xlsx";
import { Cards, Users, User_history, Comment } from "../util/models";
import { hashPassword } from "../util/hash";

dotenv.config();

// const knexConfigs = require("../knexfile");
// const configMode = process.env.NODE_ENV || "development";
// const knexConfig = knexConfigs[configMode];
// const knex = KnexInit(knexConfig);

export async function seed(knex: Knex): Promise<void> {
	const txn = await knex.transaction();
	try {
		let workbook = XLSX.readFile("./seeds/seed_data.xlsx");
		let cardsWs = workbook.Sheets["cards"];
		let usersWs = workbook.Sheets["users"];
		let user_historyWs = workbook.Sheets["user_history"];
		let commentWs = workbook.Sheets["comment"];

		let cards: Cards[] = XLSX.utils.sheet_to_json(cardsWs);
		let users: Users[] = XLSX.utils.sheet_to_json(usersWs);
		let user_history: User_history[] = XLSX.utils.sheet_to_json(user_historyWs);
		let comment: Comment[] = XLSX.utils.sheet_to_json(commentWs);

		// console.log(cards)
		for (let user of users) {
			user.password = await hashPassword(user.password!.toString());
		}
		// console.log(user_history)
		// console.log(comment)

		// Deletes ALL existing entries + reset ID
		await txn("comments").del();
		await txn.raw("ALTER SEQUENCE comments_id_seq RESTART");
		await txn("user_history").del();
		await txn.raw("ALTER SEQUENCE user_history_id_seq RESTART");
		await txn("users").del();
		await txn.raw("ALTER SEQUENCE users_id_seq RESTART");
		await txn("card_images").del();
		await txn.raw("ALTER SEQUENCE card_images_id_seq RESTART");
		await txn("card_prices").del();
		await txn.raw("ALTER SEQUENCE card_prices_id_seq RESTART");
		await txn("card_sets").del();
		await txn.raw("ALTER SEQUENCE card_sets_id_seq RESTART");
		await txn("cards").del();
		await txn.raw("ALTER SEQUENCE cards_id_seq RESTART");

		// Insert cards
		for (let card of cards) {
			let eachCard = {
				api_id: card.id,
				name: card.name,
				type: card.type,
				desc: card.desc,
				atk: card.atk,
				def: card.def,
				banlist: card.banlist_info,
				level: card.level,
				race: card.race,
				attribute: card.attribute,
				linkval: card.linkval,
				linkmarkers: card.linkmarkers,
				archetype: card.archetype,
				scale: card.scale,
			};
			let cardId = await txn("cards").insert(eachCard).returning("id");

			// Insert card_sets
			let card_sets = card.card_sets;
			if (card_sets) {
				card_sets = (card_sets as any).replaceAll(": '", ': "');
				card_sets = (card_sets as any).replaceAll("',", '",');
				card_sets = (card_sets as any).replaceAll("{'", '{"');
				card_sets = (card_sets as any).replaceAll("'}", '"}');
				card_sets = (card_sets as any).replaceAll(", '", ', "');
				card_sets = (card_sets as any).replaceAll("':", '":');
				let card_sets_JSON = JSON.parse(card_sets as string);
				for (let card_set_JSON of card_sets_JSON) {
					let each_card_set_JSON = {
						card_id: cardId[0].id,
						name: card_set_JSON.set_name,
						code: card_set_JSON.set_code,
						rarity: card_set_JSON.set_rarity,
						price: card_set_JSON.set_price,
					};
					await txn("card_sets").insert(each_card_set_JSON);
				}
			} else {
				// console.log("card_sets seed failed");
			}

			// Insert card_price
			let card_prices = card.card_prices;
			if (card_prices) {
				card_prices = (card_prices as any).replaceAll("'", '"');
				let card_prices_JSON = JSON.parse(card_prices as unknown as string);
				for (let card_price_market in card_prices_JSON[0]) {
					let each_card_price_JSON = {
						card_id: cardId[0].id,
						card_market: card_price_market,
						price: card_prices_JSON[0][card_price_market],
					};
					await txn("card_prices").insert(each_card_price_JSON);
				}
			} else {
				console.log("card_prices seed failed");
			}

			// Insert card_images
			let card_images = card.card_images;
			if (card_images) {
				card_images = (card_images as any).replaceAll("'", '"');
				let card_images_JSON = JSON.parse(card_images as unknown as string);
				let each_card_image_JSON = {
					card_id: cardId[0].id,
					image_url: card_images_JSON[0].image_url,
					small_pic: card_images_JSON[0].image_url_small,
				};
				await txn("card_images").insert(each_card_image_JSON);
			} else {
				console.log("card_images seed failed");
			}
		}

		// Insert user
		let userId = await txn("users").insert(users).returning("id");
		console.log("userID:", userId);

		// Insert user_history
		for (let history of user_history) {
			let user_id = await txn.select("id").where("username", history.user).from("users");
			let cardId = await txn.select("id").where("name", history.card).from("cards");
			let u_histroy = {
				user_id: user_id[0].id,
				card_id: cardId[0].id,
				price: history.price,
				buyer_id: history.buyer_id,
				seller_id: history.seller_id,
			};
			await txn("user_history").insert(u_histroy);
		}

		// Insert comment
		for (let com of comment) {
			let user_id = await txn.select("id").where("username", com.user).from("users");
			let cardId = await txn.select("id").where("name", com.card).from("cards");
			let user_comment = {
				user_id: user_id[0].id,
				card_id: cardId[0].id,
				comment: com.comment,
			};
			await txn("comments").insert(user_comment);
		}

		await txn.commit();
	} catch (error) {
		console.log(error);
		await txn.rollback();
	}
}
